#include <iostream> 
#include <string>

using std::string;
using std::cin;
using std::cout;
using std::endl;

int main(){
    string line;
    cin >> line;
    cout << "Line: " << line << endl;
}
