/*
 * File: functions_hw7a.h
 * Content: definition of triagram class methods and other functions for
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#ifndef FUNCTIONS_HW7A_H
#define FUNCTIONS_HW7A_H

#include <iostream>
#include <string>
#include <map>

using std::string;
using std::map;

class trigram {

    public: 
        
        //Define default constructor:
        trigram(): str2("<START_1>"), str3("<START_2>") {}
        
        //method to concatenate the three strings currently
        string concat() const;

        //method to update words stored in an object
        void update(string new_str3);

        void endTri();

        //declare getter methods for unit testing
        string getstr1();
        string getstr2();
        string getstr3();

    private:
        string str1;
        string str2;
        string str3;
       
};

void printMap(map<string, int> *trigrams);
#endif
