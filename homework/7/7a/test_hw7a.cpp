/*
 * File: test_hw7a.cpp
 * Content: unit test of functions used in part A of HW7
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#include <iostream>
#include <string>
#include <map>
#include "functions_hw7a.h"
#include <cassert>

using std::cout;
using std::endl;
using std::string;

void test_trigram();

int main(){
    test_trigram();

    cout << "Passed all tests!" << endl;
    return 0;
}

void test_trigram() {
//tests if triagram class and its members behave properly.
    
    //TEST 1: check if members of objects are initialized as specified:
    trigram test_tri;
    assert(test_tri.getstr1().empty());
    assert(test_tri.getstr2() == "<START_1>");
    assert(test_tri.getstr3() == "<START_2>");
    
    //TESTS 2: check if update() method is properly working:
    test_tri.update("Added string");
    assert(test_tri.getstr1() == "<START_1>");
    assert(test_tri.getstr2() == "<START_2>");
    assert(test_tri.getstr3() == "Added string");

    //TEST 3: check if concat() method is propertly working
    string concat = test_tri.concat();
    assert(concat == "<START_1> <START_2> Added string");
}
