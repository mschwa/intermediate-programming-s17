/*
 * File: Afuncs.h
 * Content: triagram class definition and declaration of functions in Afuncs.c
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#ifndef AFUNCS_H
#define AFUNCS_H

#include <iostream>
#include <string>

using std::string;

class triagram {

    public:
        string str1;
        string str2;
        string str3;
        
        //Define default constructor:
        triagram() : str1("<START_1>"), str2("<START_2>") { } 

        string concat(); 
        //method to concatenate the three strings currently stored in object

        void updateTri(string new_str3);
        //mehtod to update words stored in object
        
        void endTri();
}


#endif
