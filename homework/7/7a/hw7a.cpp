/*
 * File: hw7a.cpp
 * Content: definition of triagram class methods and other functions for
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#include <iostream>
#include <string>
#include <map>
#include <fstream> //library to read from files
#include "functions_hw7a.h"

using std::cin;
using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::map;

int main(int argc, char *argv[]){
    if(argc != 3){
        cout << "Invalid number of inputs." << endl;
    }

    map<string, int> trigram_freq;

    //declare variables that will be used to read files and form trigrams
    ifstream headfile; //file containing the name of the multiple files to be read
    string filename; //stores the name of the text files to be read from headfile
    ifstream textfile; //file handle for the individual text files to be read
    string word; //stores words read from file
    string sentence; //stores the 3-word trigrams being read
    trigram *ptrTri;

    //open head file
    headfile.open(argv[1]); 
    //get the name of the files
    while(headfile >> textfile){
        //open file that contains actual tests
        textfile.open(filename);
        //create pointer to triagram object so that new object is created for
        //each file to be read
        trigram *triPtr = new trigram();
        while(textfile >> word){
            triPtr->update(word);
            sentence = triPtr->concat();
            ++trigram_freq[sentence];
        }
        //include trigrams with end tags
        triPtr->update("<END_1>");
        sentence = triPtr->concat();
        ++trigram_freq[sentence];
        triPtr->update("<END_2>");
        sentence = triPtr->concat();
        ++trigram_freq[sentence];
        //delete trigram object in memory
        textfile.close();
        textfile.clear();
        delete triPtr;
    }
    headfile.close();
    printMap(&trigram_freq);


}
