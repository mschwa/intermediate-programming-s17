/*
 * File: Afuncs.cpp
 * Content: definition of triagram class methods and other functions for
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#include <iostream>
#include <string>

using std::string;

string triagram::concat() const { 
    //method to concatenate the three strings currently stored in object
    string tri;
    tri = str1 + str2 + str3;
    return tri;
}

void triagram::updateTri(string new_str3){
    //mehtod to update words stored in object
    str1 = str2;
    str2 = str3;
    str3 = new_str3;
}

void triagram::endTri(){
    //includes end tags in triagrams
    updateTri("<END_1>");
    updateTri("<END_2>");

}
