/*
 * File: functions_hw7a.cpp
 * Content: definition of triagram class methods and other functions for
 *
 * Marcos Schwartz, mschwa69
 * 04/16/2017
*/

#include <iostream>
#include <string>
#include <map>
#include "functions_hw7a.h"

using std::string;
using std::map;

string trigram::concat() const { 
    //method to concatenate the three strings currently stored in object
    string tri;
    tri = str1 + " " + str2 + " " +  str3;
    return tri;
}

void trigram::update(string new_str3){
    //mehtod to update words stored in object
    str1 = str2;
    str2 = str3;
    str3 = new_str3;
}

void trigram::endTri(){
    //includes end tags in triagrams
    update("<END_1>");
    update("<END_2>");
}

string trigram::getstr1(){
    //getter method for str1
    return str1;
}

string trigram::getstr2(){
    //getter method for str1
    return str2;
}

string trigram::getstr3(){
    //getter method for str1
    return str3;
}

void printMap(map<string, int> *trigrams){
    for(map<string,int>::const_iterator it = *trigrams.begin();
        it != *trigrams.end();
        ++it){
        cout << it->first << " " << it->second << endl;
    }
        
}
