/* File: hw3a.c`
 * Homework 3, 600.120 Spring 2017
 *
 * Marcos Schwartz, mschwa69
 * 2/27
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void printInfo(FILE* output, char course[],unsigned int code);
int binary(unsigned int num);
void  encode(unsigned int* pointCode, unsigned int num, int n);
int match(unsigned int* pointCode,char course[]);


int main(int argc, char* argv[]) {
    
    // Confirm that a command-line argument is present
    if (argc == 1) {
        printf("Program requires file input.\n");
        return 1; // exit program because no command line argument is present
    }
    
    // Declare file handle of inputt file
    FILE* input = fopen(argv[1],"r");
    FILE* output = fopen(argv[2],"w");
    // Declare the array of characters that will store the file's lines as assigned by
    // fscanf
    unsigned int code=0;
    char course[16];
    while(fscanf(input,"%s",course) != EOF){
        match(&code,course);
        printInfo(output,course,code);
        code = 0;        
    } 

    //CLOSE FILE HANDLES
    fclose(input);
    fclose(output);
    return 0;
}

void printInfo(FILE* output, char course[],unsigned int code){
//prints course information, binary form and unsigned integer form to the screen. Writes
//unsigned integer form to output file.
    printf("%s  ",course);
    binary(code);
    printf("  %u\n",code);
    fprintf(output,"%u\n",code);
}

int binary(unsigned int num){
//prints binary representaion of passed unsigned integer num.
    unsigned int c = 1;
    char binary_number[33];
    binary_number[32] = '\0';
    for(int i=31; i>=0; i--){
        if (c&num){
            binary_number[i]='1';
        } else {
            binary_number[i]='0';
        }
        num >>= 1;
    }
    printf("%s",binary_number);
    return 0;
}

void encode(unsigned int* pointCode, unsigned int num, int n){
//function builds code using bit-wise OR and shift operations. Shift should be done
//by the number of bits occupied by the enconded information that will next be 
//encoded.
*pointCode |= num;
*pointCode <<= n;
}

int match(unsigned int* pointCode,char course[]) {
//matches portions of the course information to their unsgined integers 
//correspondents.
    //define strings containing the number correspondence of codes (only last letter
    //for division code is necessary since that letter is unique)
    char div_letter[8] = {'E','U','D','N','S','H','Y','A'}; 
    char grade[8] = {'A','B','C','D','F','I','S','U'};
    char sign[3] = {'+','-','/'};

    //Getting division number
    char div = course[1];
    int i = 0;
    while(div != div_letter[i]){
        i += 1; 
    }
    //encoding it
    encode(pointCode,i,10);

    //Getting the deparment number
    unsigned int p = 2;
    unsigned int dept = 0;
    for (i=3;i<6;i++){
        //convert number character in course info to integer by subtracting '0'
        dept += (course[i]-'0')*pow(10,p);
        p--;
    }
    encode(pointCode,dept,10);

    //Getting course number  
    p = 2;
    unsigned int courseNum = 0;
    for (i=7;i<10;i++){
        courseNum += (course[i]-'0')*pow(10,p);
        p--;
    }
    encode(pointCode,courseNum,3);

    //Getting grade letter:
    char letter = course[10];
    i = 0;
    while(letter != grade[i]){
        i += 1; 
    }
    encode(pointCode,i,2);

    //Getting grade sign:
    i = 0;
    char symb = course[11];
    while(symb != sign[i]){
        i += 1; 
    }
    encode(pointCode,i,3);

    //Getting the number of credits:
    if (course[14]=='0'){
        encode(pointCode,course[12]-'0',1);
    }
    else {
        encode(pointCode,course[12]-'0',1);
        encode(pointCode,1,0);
    }
    return 0; 
}




