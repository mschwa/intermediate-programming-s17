/* File: hw3b.c`
 * Homework 3, 600.120 Spring 2017
 *
 * Marcos Schwartz, mschwa69
 * 2/27
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

int decipher(unsigned int code);
unsigned int decode(unsigned int code,int offset, int n);
unsigned int maskGen(int n);

// Menu functions
void printMenu(){
    printf(
            "n - display the total number of courses\n"
            "d - list all courses from a particular department\n"
            "l - list all courses with a particular letter grade\n"
            "c - list all courses with at least a specified number of credits\n"
            "g - compute the GPA of all courses with letter grades\n"
            "q - quit the program\n"
            "Enter letter choice ->\n");
}

void promptN(int i) {
    printf("Total number of courses: %d\n",i);
}

void promptD() {
    printf("Enter the department: \n");
}

void promptL(){
    printf("Enter a letter grade and mark (+,-,/): \n");
}

void promptC() {
    printf("Enter a number of credits: \n");
}

void promptG(float GPA){
    printf("GPA: %.3f\n",GPA);
}

void noMatches() {
    printf("No matches\n");
}


int main(int argc, char* argv[]) {
    
    // Confirm that a command-line argument is present
    if (argc == 1) {
        printf("Program requires file input.\n");
        return 1; // exit program because no command line argument is present
    }
    
    // Declare file handle of inputt file
    FILE* input = fopen(argv[1],"r");
    // Declare the array of characters that will store the file's lines as assigned by
    // fgets
    unsigned int codes[2500];
    int i = 0;
    while (fscanf(input,"%u",&codes[i])!=EOF){
        //while inputs is being read, the integer read is assigned to code.
        //decipher(codes[i]);
        i++;
    }
    
    //define arrays that will be used during queries
    int cond = 1;
    char c;
    char grade[8] = {'A','B','C','D','F','I','S','U'};
    char sign[3] = {'+','-','/'};
    //start iterativity
    while (cond){
        if(c!='\n'){
            printMenu();
        }
        c = getchar();
        //if user enters n
        if (c == 'n' || c== 'N'){
            promptN(i);

        }
        //if user enters d
        else if (c=='d' || c=='D'){
            //prompts for input of department and reads it
            promptD();
            unsigned int dept;
            scanf("%u",&dept);
            //loops through courses looking for matches through decode
            for(int c=0;c<i;c++){
                if(dept==decode(codes[c],19,10)){
                    decipher(codes[c]);
                }
            }

        //if user enters l
        } else if (c=='l' || c=='L'){
            //prompts for letter grade
            promptL();
            char letter[3]; letter[2]='\0';
            scanf("%s",letter);
            if (strlen(letter)==1){
                while (strlen(letter)==1){
                    printf("Provide letter grade and sign: ");
                    scanf("%s",letter);
                }
            }
            //matches passed characters to their correspondent unsigned integers
            unsigned int letter_index = 0;
            unsigned int sign_index =0;
            for(unsigned int c=0;c<8;c++){
                if(toupper(letter[0])==grade[c]){
                    letter_index=c;
                    for(unsigned int j=0;j<3;j++){
                        if(letter[1]==sign[j]){
                            sign_index=j;
                        }
                    }
                }
            }
            //loop through the code checking if they match the grades and signs of 
            //courses in the database      
            for(int c=0;c<i;c++){
                if((letter_index == decode(codes[c],6,3)) && (sign_index == decode(codes[c],4,2))){
                    decipher(codes[c]);
                }
            }

        }
        //if user enters c
        else if (c == 'c' || c =='C'){
            promptC();
            float num;
            scanf("%f",&num);
            // convert the number of credits of each course to float and compares it to
            // the input float for printing
            for (int c=0;c<i;c++){
                float int_part = (float)decode(codes[c],1,3);
                float dec_part=0;
                if (decode(codes[c],0,1)){
                    dec_part=0.5;
                }
                if ((dec_part + int_part) >= num){
                    decipher(codes[c]);
                }
            }

        }
        //if user enters g
        else if (c == 'g' || c == 'G'){
            //define variables used for GPA calculation
            float sum_weights=0;
            float GPA=0;
            float scale[6] = {4.0,3.0,2.0,1.0,0.0,0.0};
            //loop through courses
            for(int c=0;c<i;c++){
                //extract the letter grade and number of credits from each course
                unsigned int letter = decode(codes[c],6,3);
                unsigned int symbol = decode(codes[c],4,2);
                unsigned int int_part = decode(codes[c],1,3);
                unsigned int dec_part = decode(codes[c],0,1);
                //skip I, S, U grades
                if((letter != 5) && ( letter !=6) && (letter != 7)){
                    //assign letter grade part to variable storing the grade
                    float grade = scale[letter];
                    //if letter is E or F, there should be no + or -, so do nothing
                    if (letter == 4 || letter == 5){} 
                    //else consider the signal in the grade value
                    else {
                        if (symbol==0 && letter!=0){
                            //+ adds 0.3 to "unsigned" grade
                            grade+=0.3;
                        } else if (symbol==1){
                            //- subtracts 0.3 from "unsigned" letter grade
                            grade -= 0.3;
                        }
                    }
                    float weight;
                    //weight of current course equals its number of credits
                    if (dec_part == 0 && letter!=3){
                        weight = (float)int_part;
                    } else {
                        weight = (float)int_part + 0.5;
                    }
                    GPA += weight*grade;
                    sum_weights += weight;
                } 
            }
            //find final GPA by dividing current sum by the weights
            GPA /= sum_weights;
            promptG(GPA);

        }
        //if user enters q 
        else if (c == 'q' || c == 'Q'){
            cond = 0;
            printf("\n");
        } else if (c=='\n'||c==EOF){       
        } else {
            printf("No matches.\n");
        } 
    }

    //CLOSE FILE HANDLES
    fclose(input);
    return 0;
}

unsigned int decode(unsigned int code,int offset, int n){
//function performs the bit-wise AND operation between the variable the holds
//the code and the argument and a mask. It then pushes the bits in the unsigned int
//n positions to the right to allow further addition of code. Returns the unsigned
//int containing the correspondent code. 
    code = code >>  offset;
    unsigned int portion = code & maskGen(n);
    //uncomment 2 lines below for debugging
    //binary(point_code);
    //printf("\n");
    return portion;
}

unsigned int maskGen(int n){
//function generates mask that can be &-ed to code in order to obtain specific section
//of the code. It takes the number n of bits used to hold the specific portion wanted.
    unsigned int mask = 0;
    for (int i = 0;i<n;i++){
        mask += pow(2,i);
    }
    return mask;
}


int decipher(unsigned int code){
//function receives course information as first argument and pointer to the variable
//that will store the final code as the second one.

    //define strings containing the number correspondence of codes (only last letter
    //for division code is necessary since that letter is unique)
    char div_letter[16] = {'M','E','B','U','E','D','E','N','A','S','P','H','P','Y','S','A'}; 
    char grade[8] = {'A','B','C','D','F','I','S','U'};
    char sign[3] = {'+','-','/'};
    char digits[10]={'0','1','2','3','4','5','6','7','8','9'};

    //define the char array that will store the course info:
    char course[16];
    course[2]='.';course[6]='.';course[13]='.';
    course[15]='\0';

    // Decode credits:
    if (decode(code,0,1)==0){ //if last bit is zero
        course[14]='0';
        course[12]=digits[decode(code,1,3)];
    }
    else {
        course[14]='5';
        course[12]=digits[decode(code,1,3)];
    }
    
    //Get the number correspondent to grade sign and decode it
    course[11]= sign[decode(code,4,2)];

    //Get number correspondent of grade:
    course[10] = grade[decode(code,6,3)];

    //obtain course number as an integer 
    unsigned int num = decode(code,9,10);
    for (int i=9;i>6;i--){
        course[i] = digits[num%10];
        num /= 10;
    }

    // encode number:
    unsigned int dept = decode(code,19,10);
    for (int i=5;i>2;i--){
        course[i] = digits[dept%10];
        dept /= 10;
    }
      
    //Read dividision number
    int c = 2*decode(code,29,3);
    course[0]=div_letter[c];
    course[1]=div_letter[c+1];

    printf("%s\n",course);
    return 0;
}




