/* Homework 6, 600.120, Spring 2017
 * File: menuUtil.h
 * Content: declaration of all functions related to the functioning of the menu.
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#ifndef MENUUTIL_H
#define MENUUTIL_H

#include "imageManip.h" //for struct definition

void menuPrompt();
//Displays the menu to user.

void noImageError();
//Prints error when user tries to perform operation without providing a ppm file.

void inputNumError();
//Prints error message when user provides inadequate input to perform an action

int wordCount(char *string);
//counts number of words in string input by user

pixel *rCall(int *columns, int *rows, int *colors, char* filename);
//Function called when user enters "r" command

void wCall(pixel *picPixels, int *columns, int *rows, int *colors, char *filename);
//Function called when user enters "w" command.

#endif
