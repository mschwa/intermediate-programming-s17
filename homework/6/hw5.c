/* Homework 5, 600.120, Spring 2017
 * File: hw5.c
 * Content: main function that initiates the program.
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#include "catalog.h"
#include "prompts.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]){
    if (argc == 1){
        printf("Provide catalog text file.\n");
        return -1;
    }
    
    //Open and validate input file. Create catalog array if it is valid.
    FILE* input = fopen(argv[1],"r");
    genCourse *catalog = validateCatalog(input);
    if (catalog == NULL){
        return -1;
    }


    //Start interaction
    const char actions[10] = {'q','1','2','3','4','5','6','7','8','9'};
    int run = 1;
    char command = '0';
    while (run){
        //case for each input
    }

    printCourse(9,catalog);
    free(catalog);
    fclose(input);
    return 0;
}
