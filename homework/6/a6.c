/* Homework 6, 600.120, Spring 2017
 * File: a6.c
 * Content: main() function
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

int main() {
    //initialize variables that will hold the number of columns, rows and color
    //shades held by each pixel
    int *columns = malloc(sizeof(int));
    int *rows = malloc(sizeof(int));
    int *colors = malloc(sizeof(int));
    *columns = 0;
    *rows = 0;
    *colors = 0;

    //Start interaction
    int run = 1;
    char commandline[101];
    char *filename;
    char *letter;
    pixel *picPixels = NULL;
    while (run) {
        menuPrompt();
        fgets(commandline,100,stdin);
        int numArgs = wordCount(commandline);
        letter = strtok(commandline," ");
        switch(*letter) {
        case 'r':
            if (numArgs != 2) {
                inputNumError();
            } else {
                if (picPixels) {
                    free(picPixels);
                }
                //reset value of the following variables to read new file correctly.
                *columns = 0;
                *rows = 0;
                *colors = 0;
                filename = strtok(NULL, " ");
                //change read newline character in filename by null terminator
                *(filename + strlen(filename) - 1) = '\0';
                picPixels = rCall(columns,rows,colors,filename);
            }
            break;
        case 'w':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 2) {
                inputNumError();
            } else {
                filename = strtok(NULL, " ");
                //change read newline character in filename by null terminator
                *(filename + strlen(filename) - 1) = '\0';
                wCall(picPixels,columns,rows,colors,filename);
            }
            break;
        case 'c':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 5) {
                inputNumError();
            } else {
                int c1 = atoi(strtok(NULL," "));
                int r1 = atoi(strtok(NULL," "));
                int c2 = atoi(strtok(NULL," "));
                int r2 = atoi(strtok(NULL," "));
                pixel *cropPtr = crop(picPixels,c1,r1,c2,r2,columns,rows);
                if (cropPtr) {
                    picPixels = cropPtr;
                }
            }
            break;
        case 'i':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 1) {
                inputNumError();
            } else {
                invert(picPixels,columns,rows);
            }
            break;
        case 's':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 1) {
                inputNumError();
            } else {
                swap(picPixels,columns,rows);
            }
            break;
        case 'g':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 1) {
                inputNumError();
            } else {
                grayscale(picPixels,columns,rows);
            }
            break;
        case 'b':
            if(!picPixels) {
                noImageError();
            } else if (numArgs != 2) {
                inputNumError();
            } else {
                double delta = atof(strtok(NULL," "));
                brightness(picPixels,columns,rows,delta);
            }
            break;
        case 'q':
            if (numArgs != 1) {
                inputNumError();
            } else {
                run = 0;
            }
            break;
        case '\n':
        case EOF:
            break;
        default:
            printf("Error: Unknown command\n");
        }
    }

    //AT END OF APPROPRIATE INTERATION, RESET VALUES OF COLUMNS, ROWS AND COLORS

    free(columns);
    free(rows);
    free(colors);
    free(picPixels);
    //free picPixels
    return 0;

}
