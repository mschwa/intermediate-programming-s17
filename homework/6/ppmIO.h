/* Homework 6, 600.120, Spring 2017
 * File: ppmIO.h
 * Content: declaration of all functions to carry out the necessary file I/O operations.
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */


#ifndef PPMIO_H
#define PPMIO_H

#include <stdio.h>
#include <stdlib.h>
#include "menuUtil.h"
#include "imageManip.h"


void skipComments(FILE* picture);
//skips comments in ppm file

pixel* checkFile(FILE *picture, int *columns, int *rows, int *colors);
//Checks if file is in valid ppm format. If it is, function creates array of pixel
//structs. Otherwise, it returns NULL.

void write2file(pixel *picPixels, FILE* fileOut, int *columns, int *rows, int *colors);
//Writes properly formatted ppm file with to a givne file.


#endif
