/* Homework 6, 600.120, Spring 2017
 * File: menuUtil.c
 * Content: implementation of all functions related to the functioning of the menu.
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

void menuPrompt() {
//Function simply displays the menu to the user
    printf("Main menu:\n"
           "        r <filename> - read image from <filename>\n"
           "        w <filename> - write image to <filename>\n"
           "        c <x1> <y1> <x2> <y2> - crop image to the box with the given corners>\n"
           "        i - invert intensities\n"
           "        s - swap color channel\n"
           "        g - convert to grayscale\n"
           "        b <amt> - change brightness (up or down) by the given amount\n"
           "        q - quit\n"
           "Enter choice: ");
}


//write a function for each function call. In each function, make sure file used
//before it has been closed.

int wordCount(char *string) {
//function clears buffer before new input is collected
    char copy[101];
    strcpy(copy,string);
    int numWords = 1;
    strtok(copy, " ");
    while(strtok(NULL," ") != NULL) {
        numWords++;
    }
    return numWords;
}

void noImageError() {
//Prints error message when a command for image manipulation is entered before any
//file is read.
    printf("Error: there is no current image.\n");
}

void inputNumError() {
//Prints error message when user doesn't provide the right number of arguments
//required to perform a command
    printf("Error: follow the format in the menu to perform this command.\n");
}

pixel *rCall(int *columns, int *rows, int *colors, char *filename) {
//Function called then user enters "r" command
    FILE *picture = fopen(filename,"r");
    if(!picture) {
        printf("Error: file %s not found\n",filename);
        return NULL;
    } else {
        //once existing file is provided, check if it is in the valid ppm format.
        pixel *picPixels = checkFile(picture,columns,rows,colors);
        if(!picPixels) {
            printf("File is not in (valid) ppm format.\n");
            fclose(picture);
            return NULL;
        }
        printf("Reading from %s...\n",filename);
        //once appropriate file is obtained, return pixels array
        fclose(picture);
        return picPixels;
    }
}

void wCall(pixel *picPixels, int *columns, int *rows, int *colors, char *filename) {
//Function called when user enters "w" command.
    FILE *fileOut = fopen(filename,"w");
    while(!fileOut) {
        printf("Invalid name. Try again.\n");
        //change read newline character in filename by null terminator
        *(filename + strlen(filename) - 1) = '\0';
        fgets(filename,100,stdin);
        fileOut = fopen(filename,"w");
    }
    printf("Writing to %s...\n",filename);
    write2file(picPixels,fileOut,columns,rows,colors);
    fclose(fileOut);
}
