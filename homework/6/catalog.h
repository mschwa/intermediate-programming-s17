/* Homework 5, 600.120, Spring 2017
 * File: catalog.h
 * Content: declaration of functions in catalog.c
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#ifndef CATALOG_H
#define CATALOG_H

#include <stdio.h>
#include <stdlib.h>

// Define struct where information about course will be stored
typedef struct GeneralCourseInfo {
    char div[3]; //space for null terminator
    char dpt[4];
    char num[4];
    float cred;
    char title[33]; //space for null terminator
} genCourse; 

//Define struct where student information regarding a course will be stored.
typedef struct StudentCourseInfo {
    genCourse course;
    char semester[6]; //text representation of semester in yyyy.s format
    char grade[3]; //text representation of grade in Gg format
    struct StudentCourseInfo *next; //recursive definition of pointer for linked list   
} stuCourse; 

char** checkListing(char* listing);
//checks validity of course listing. If listing is valid, returns array pointing to it

int isfloat(char *string);
//checks if string is in c.f format

genCourse* validateCatalog(FILE* input, int* courseCount);
//validates input catalog file and creates course array if valid catalog is given.

void printCourse(genCourse* catalog);
//prints complete course information to screen.

void printCourse_file(genCourse* catalog, FILE* fileOut);
//prints complete course information to file.

void dispCat(genCourse* catalog, int* courseCount, FILE* fileOut);
//2: prints catalog array.

int matchCatalog(char* courseID, genCourse* catalog, int* courseCount);
//matches provided listing (courseID) to course in catalog array

int newInput(char* curInput, genCourse* catalog, int* courseCount);
//indicates whehter it is necessary to ask user for new valid course listing.

void dispInfo(genCourse* catalog, int* courseCount);
//prints information about a specific course to screen.

genCourse* promptINput(genCourse* catalog, int* courseCount);
//prompts user to provide course listing while invalid input is entered. Then, returns
//position of matching course in catalog.

void updateTitle(genCourse* catalog, int* courseCount);
//3: updates the title of a course specified by user.

void updateCred(genCourse* catalog, int* courseCount);
//4: updates the credit assignmetn of a course specified by user.

#endif
