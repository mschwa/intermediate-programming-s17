/* Homework 5, 600.120, Spring 2017
 * File: catalog.c
 * Content: definition of structures and functions used for course cataloguing and 
 *          transcript representation.
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "catalog.h" //for struct declarations
#include "prompts.h"

char** checkListing(char* listing){
//Checks validity of course listing. If it is valid, returns  pointer to array 
//containing course division, department and number. Otherwise, returns NULL pointer
    int valid = 1;
    //Copy course listing to use strtok and validate course division, department, 
    //and number.
    char listingCopy[strlen(listing)];
    strcpy(listingCopy,listing);
    //Division must be a two-character string
    char* division = strtok(listingCopy,".");
    if (strlen(division) > 2){
        valid = 0;
    }
    //Department and course number must be three-digit numbers:
    char *department = strtok(NULL,".");
    int dpt_int = atoi(department);
    char *number = strtok(NULL,".");
    int num_int = atoi(number);
    if (dpt_int/1000 > 0 || num_int/1000 > 0 || !dpt_int || !num_int){
        valid = 0;
    }
    if (valid){
        char **ID = malloc(3*sizeof(char *));
        ID[0] = malloc(sizeof(division));
        ID[1] = malloc(sizeof(department));
        ID[2] = malloc(sizeof(number));
        strcpy(ID[0], division);
        strcpy(ID[1], department);
        strcpy(ID[2], number);
        return ID;
    } else {
        return NULL;
    }
}

int isfloat(char *string){
//checks if string is in c.f format. Returns 1 if its is, 0 otherwise.
    int dotCount = 0;
    int letterCount = 0;
    int len = strlen(string);
    for (int i = 0; i < len; i++){
        if (string[i] == '.'){
            //keep track of all '.' characters in string
            dotCount++;
        } 
        if (isalpha(string[i])){
            //check if there's any letter in string
            letterCount++;
        }
    }
    if ((dotCount == 1) && (!letterCount) && (string[len-1]!='.')){
        //if there's only one dot and no numbers in string, it is in the c.f format
        return 1;
    } else {
        return 0;
    }
}

genCourse* validateCatalog(FILE* input, int* courseCount){
//Validates input catalog files. If valid catalog is provided, function returns
//catalogue array. Otherwise, it retuns a NULL value.
    unsigned int valid = 1;
    char line[1001]; 
    char lineCopy[1001]; //copy of line used by strtok
    int size = 20;
    genCourse *catalog = malloc(size*sizeof(genCourse));
    while ( fgets(line,1000,input) != NULL ) {
        if (line != NULL){
            // Remove newline character of read line:
            line[strlen(line)-1] = '\0';
        } else {
            valid = 0;
        }
        // Make copy of line that can be modified by strtok function
        strcpy(lineCopy,line);
        // Use strtok to read course listing (fist token)
        char *listing = strtok(lineCopy," ");
        //Check validity of credit assignment
        char *credits = strtok(NULL, " ");
        if (!isfloat(credits)){
            //if credit assignment isn't in c.f format
            valid = 0;
        }
        //use strcpy to collect the course title. Copy starts at position 1 +
        //strlen(credits) + 1,that is, one character after the end of c.f
        char title[34];
        title[33]='\0';
        strncpy(title,(line+11+strlen(credits)+1),33);
        char division[3];
        char department[4];
        char number[4];
        if (strlen(title) == 33){
            //if title contains more than 32 characters
            valid = 0;
        } else {
            //call checklisting to validate course listing information
            char **listingInfo = checkListing(listing);
            if (listingInfo == NULL) {
                valid = 0;
            } else {
                //if listing is valid, use its values
                strcpy(division,listingInfo[0]);
                strcpy(department,listingInfo[1]);
                strcpy(number,listingInfo[2]);
                //listingInfo is only populated if listing is valid, so free
                //must be done at this block
                for (int i = 0; i<3;i++){
                    free(listingInfo[i]);
                }
            }
            free(listingInfo);
        }
        if (valid) {
            //if input file contains more than 20 courses, update size of catalog
            if (*courseCount == size){
                size *= 2;
                catalog = (genCourse*)realloc(catalog,size*sizeof(genCourse));
            }
            //use information of valid course to populate the catalog array
            strcpy((catalog+*courseCount)->div, division);
            strcpy((catalog+*courseCount)->dpt, department);
            strcpy((catalog+*courseCount)->num, number);
            (catalog+*courseCount)->cred = atof(credits);
            strcpy((catalog+*courseCount)->title, title);
        } else {
            //if input file is invalid, free catalog and end validation
            free(catalog);
            return NULL;
        }
        //update couting of courses
        (*courseCount)++;
     }
    //adjust size of catalog according to number of courses provided
    catalog = (genCourse*)realloc(catalog,(*courseCount)*sizeof(genCourse));
    return catalog;
}

void printCourse(genCourse* catalog){
//Prints complete course information to screen in the xx.ddd.nnn c.f Course Title\n 
//format.
    printf("%s",catalog->div);
    printf(".%s.",catalog->dpt);
    printf("%s ",catalog->num);
    printf("%.1f ",catalog->cred);
    printf("%s\n",catalog->title);
}

void printCourse_file(genCourse* catalog, FILE* fileOut){
//Prints complete course information in the xx.ddd.nnn c.f Course Title\n 
//format to a file.
    fprintf(fileOut,"%s",catalog->div);
    fprintf(fileOut,".%s.",catalog->dpt);
    fprintf(fileOut,"%s ",catalog->num);
    fprintf(fileOut,"%.1f ",catalog->cred);
    fprintf(fileOut,"%s\n",catalog->title);
}

void dispCat(genCourse* catalog, int* courseCount, FILE* fileOut){
//Prints complete information on all courses available. When called from the interactive
// menu, file argument will be equal to NULL, so that catalog is printed to screen.
    if (fileOut == NULL && courseCount){
        for (int i=0; i < *courseCount; i++){
            printCourse(catalog+i);
        }
    } else if (courseCount) {
        //in unittests, function will be called with file parameter to write catalog in
        //another file. This file will be used to assess if output catalog is correct.
        for (int i=0; i < *courseCount; i++){
            printCourse_file((catalog+i), fileOut);
        }
    fclose(fileOut);
    }
}

int matchCatalog(char* courseID, genCourse* catalog, int* courseCount){
//Matches course identifier to course in catalogue. If match exists, returns the
//position of the course in the catalog. Otherwise, returns -1.
    // Declare char array that will store the concatenation of course
    // information to provide listing of course read in the catalog
    char catInfo[11]; 
    char cur_div[3]; //will store current division being read
    char cur_dpt[4]; //will store current deparment being read
    char cur_num[4]; //will store current number being read
    int i;
    for(i=0 ; i < *courseCount; i++){
        //form listing of course being currently read
        strcpy(cur_div,(catalog+i)->div);
        strcpy(cur_dpt,(catalog+i)->dpt);
        strcpy(cur_num,(catalog+i)->num);
        sprintf(catInfo,"%s.%s.%s",cur_div,cur_dpt,cur_num);
        //if course provided matches one of the courses in the catalog
        if(strcmp(catInfo,courseID) == 0){
            return i;
        }
    }
    return -1;
}

int newInput(char* curInput, genCourse* catalog, int* courseCount){
//Function assesses the necessity of asking user to input new course listing: if -2 is
//returned, user provided invalid listing; if -1 is returned, listing is valid but 
//finds no match in the catalog. Otherwise, position of matching course in the catalog
//is returned. 
    char validInput[11]; //array where valid course listing will be stored
    if (strlen(curInput) == 10){
    //if provided listing has appropriate length, proceed to check
    //the information embeded in it
        char **info = checkListing(curInput);
        if (info){
            //if input passes checkListing test (is a valid listing), put it together
            //in one string
            sprintf(validInput,"%s.%s.%s",info[0],info[1],info[2]);
            //check if input has a match in catalog
            int matchPos = matchCatalog(validInput,catalog,courseCount);
            //free array created from checkListing
            for (int i=0; i<3; i++){
                free(info[i]);
            }
            free(info); 
            return matchPos;
        } else {
            free(info);
            return -2;
        }
    } else {
        return -2;
    }
}

genCourse* promptInput(genCourse* catalog, int* courseCount){
//prompts user for input until valid course listing is provided. Returns position of
//matching course in array.
    int invalid = 1;
    char courseID[12]; //extra space to check validity of input
    int inputCheck;
    while(invalid){
        course_prompt();
        scanf("%11s",courseID); //collect listing from user
        inputCheck = newInput(courseID, catalog, courseCount);
        if (inputCheck == -2){
            invalid_input_msg();
        } else if (inputCheck == -1){
            course_absent_msg();
        } else {
            invalid = 0;
        }
    }
    return (catalog+inputCheck);
}

void dispInfo(genCourse* catalog, int* courseCount){
//Displays information on a specific course in the catalog.
    //get address of course whose information is being required by the user
    genCourse *match = promptInput(catalog, courseCount);
    //print course
    printCourse(match);
}

void updateTitle(genCourse *catalog, int* courseCount){
//Updates the title of a specific course
    //get new title from user
    char newTitle[34];
    int invalid = 1;
    while (invalid){
        new_title_prompt();
        fgets(newTitle,33,stdin);
        if (strlen(newTitle) < 33){
            invalid = 0;
        } else {
            invalid_input_msg();
        }
    }
    //get address of course whose title user wants to update
    genCourse *match = promptInput(catalog, courseCount);
    strcpy(match->title,newTitle);
}

void updateCred(genCourse *catalog, int* courseCount){
//Updates the credit assignment of a specific course
    //get new number of credits from user
    float newCred;
    int invalid = 1;
    char* float_str;
    //initialize float_str to avoid errors:
    float_str = "initialize";
    while (invalid){
        new_credit_prompt();
        //use fgets to collect user input
        if(fgets(float_str,30,stdin)){
            if(isfloat(float_str)){
                //if isfloat function confirms input is in c.f format
                invalid = 0;
                newCred = atof(float_str);
            } else {
                invalid_input_msg();
            }
        } else {
            invalid_input_msg();
        }
    }
    //get address of course whose title user wants to update
    genCourse *match = promptInput(catalog, courseCount);
    match->cred = newCred;
}

