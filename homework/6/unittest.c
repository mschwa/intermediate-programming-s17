/* Homework 6, 600.120, Spring 2017
 * File: unittest.c
 * Content: unit tests of functions used in the program
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

void test_readNwrite();
void test_wordCount();
void test_skipComments();

int main(){
    int *columns = malloc(sizeof(int));
    int *rows = malloc(sizeof(int));
    int *colors = malloc(sizeof(int));
    *columns = 0; *rows = 0; *colors = 0;

    test_skipComments();
    test_readNwrite(columns,rows,colors);
    test_wordCount();

    printf("Program passed all tests!\n");

    free(columns);
    free(rows);
    free(colors);
}

void test_skipComments(){
//test skipComments() function in file with a buch of extra comments added
    FILE *picture = fopen("UnitTests/spaceCommented.ppm","r");
    skipComments(picture);
    int columns;
    fscanf(picture,"%d",&columns);
    //make sure possible newline character after scanf is not read
    char c = fgetc(picture);
    if (c != '\n'){
        ungetc(c,picture);
    }
    //check if correct number of columns was read
    assert(columns == 836);
    skipComments(picture);
    int rows;
    fscanf(picture,"%d",&rows);
    c = fgetc(picture);
    if (c != '\n'){
        ungetc(c,picture);
    }
    //check if correct number of rows was read
    assert(rows == 666);
    skipComments(picture);
    int colors;
    fscanf(picture,"%d",&colors);
    //check if correct number of colors was read
    assert(colors == 255);
}

void test_readNwrite(int *columns, int *rows, int *colors){
//tests checkFile function from ppmIO.h

    //TEST 1: check if function properly reads pixels in nika.ppm. Write them on
    //UnitTests/Test1.ppm file to check if all pixels were read.
    FILE *picture = fopen("UnitTests/nika.ppm","r");
    //create file where read pixels will be copied
    FILE *result = fopen("UnitTests/Test1.ppm","w");
    pixel *output = checkFile(picture,columns,rows,colors);
    //check array of pixel structs was created
    assert(output);
    //write all meaningful information extracted from the ppm file to another file
    write2file(output,result,columns,rows,colors);
    //Finalize test
    *columns = 0; *rows = 0; *colors = 0;
    free(output);
    fclose(result);
    
    //TEST 2: check if function properly reads pixels in space.ppm. Write them on
    //UnitTests/Test2.ppm file to check if all pixels were read.
    picture = fopen("UnitTests/space.ppm","r");
    result = fopen("UnitTests/Test2.ppm","w");
    output = checkFile(picture,columns,rows,colors);
    assert(output);
    write2file(output,result,columns,rows,colors);
    //Finalize test
    *columns = 0; *rows = 0; *colors = 0;
    free(output);
    fclose(result);

    //TEST 3: check if function properly rejects ppm file with invalid tag.
    picture = fopen("UnitTests/nikaInvalid1.ppm","r");
    output = checkFile(picture,columns,rows,colors);
    assert(output == NULL);

    //TEST 4: check if function properly reads pixels in file with lots of comments.
    // Write them on UnitTests/Test4.ppm file to check if all pixels were read.
    picture = fopen("UnitTests/spaceCommented.ppm","r");
    result = fopen("UnitTests/Test4.ppm","w");
    output = checkFile(picture,columns,rows,colors);
    assert(output);
    write2file(output,result,columns,rows,colors);
    //Finalize test
    *columns = 0; *rows = 0; *colors = 0;
    free(output);
}

void test_wordCount(){
//Tests wordCount function
    assert(wordCount("This is a string") == 4);
    assert(wordCount("This") == 1);
    assert(wordCount("This is a somewhat longer string") == 6);
}
