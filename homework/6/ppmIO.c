/* Homework 6, 600.120, Spring 2017
 * File: ppmIO.c
 * Content: implementations of functions to carry out the necessary file I/O operations.
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "menuUtil.h"
#include "imageManip.h"

void skipComments(FILE* picture) {
//skips comments in ppm file
    char c;
    while((c = fgetc(picture)) == '#' || c == 'P') {
        //if first character in line is a # discard all characters in line
        while((c = fgetc(picture)) != '\n') {}
    }
    //if first character in line is not a #, "unget" it so that number can be read
    ungetc(c,picture);
}

pixel* checkFile(FILE *picture, int *columns, int *rows, int *colors) {
//Checks if file is in valid ppm format. If it is, function creates array of pixel
//structs. Otherwise, it returns NULL.
    //Check if file is empty
    fseek(picture,0,SEEK_END);
    int filesize = ftell(picture);
    if (filesize < 2) {
        return NULL;
    } else {
        //if file isn't empty, return offset to beginning in order to verify
        //its content.
        rewind(picture);
    }

    char line[101];
    //First line must contain the P6 tag.
    if(fgets(line,100,picture)) {
        //replace end-of-line character by null terminator
        line[strlen(line)-1] = '\0';
        if(strcmp(line,"P6")) {
            //if text in first line isn't P6, file is in invalid format
            return NULL;
        }
    } else {
        return NULL;
    }

    char c;
    //skip comments
    skipComments(picture);
    //collect number of columns
    fscanf(picture, "%d", columns);
    c = fgetc(picture);
    if (c != '\n') {
        ungetc(c,picture);
    }
    skipComments(picture);
    //collect number of rows
    fscanf(picture, "%d", rows);
    c = fgetc(picture);
    if (c != '\n') {
        ungetc(c,picture);
    }
    skipComments(picture);
    //collect number of colors
    fscanf(picture, "%d", colors);
    //discard white space character after number of colors
    fgetc(picture);

    //having guaranteed that the file is valid and collected the necessary info,
    //create array of pixels.
    pixel *picPixels = malloc((*columns)*(*rows)*sizeof(pixel));
    fread(picPixels,sizeof(pixel),(*columns)*(*rows),picture);
    return picPixels;
}

void write2file(pixel *picPixels,FILE* fileOut,int *columns, int *rows, int *colors) {
//writes properly formatted ppm file in a file with the name specified by the user.
    //write tag, columns, rows and colors numbers to file:
    fprintf(fileOut, "P6\n%d %d\n%d\n", *columns, *rows, *colors);
    fwrite(picPixels,sizeof(pixel),(*columns)*(*rows),fileOut);
}
