/* Homework 6, 600.120, Spring 2017
 * File: imageManip.h
 * Content: declaration of all functions for image manipulation
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */


#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

typedef struct PPMpixel{
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} pixel;

pixel *crop(pixel *picPixels, int c1, int r1, int c2, int r2, int *columns, int *rows);
//Crops 

void invert(pixel *picPixels, int *columns, int *rows);
//Inverts the color pixels of an image

void swap(pixel *picPixels, int *columns, int *rows);
//Swaps color channels of pixels

void grayscale(pixel *picPixels, int *columns, int *rows);
//Uses the NTSC standard conversion formula to converts pixels to grayscale

unsigned char saturate(double product);
//guarantees that brightness operations follow sturating math

void brightness(pixel *picPixels, int *columns, int *rows, double delta);
//Adjusts image brightness by a given delta



#endif
