/* Homework 6, 600.120, Spring 2017
 * File: imageManip.c
 * Content: functions for image manipulation
 *
 * Marcos Schwartz, mschwa69
 * 04/08/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

pixel *crop(pixel *picPixels,int c1,int r1, int c2, int r2,int *columns, int *rows) {
//Crops an image from (r1,c1) to (r2,c2). If crop was
//succesful, returns array of cropped pixels. Otherwise, returns NULL.
    if ((c1 > *columns || c2 > *columns)||( r1> *rows || r2 > *rows)) {
        //if crop corners are outside the picture boundaries
        printf("Error: cropping failed, image unchanged\n");
        return NULL;
    } else if ((c1 > c2)||( r1 > r2 )) {
        //if left right corner is to the left of initial upper one
        printf("Error: cropping failed, image unchanged\n");
        return NULL;
    }
    int colCrop = (c2-c1) + 1; //number of columns in cropped picture
    int rowCrop = (r2-r1) + 1; //number of rows in cropped picture
    int numPixels = colCrop*rowCrop;
    pixel *croppedPix = malloc(numPixels*sizeof(pixel));
    //place cursor at initial cropping position
    pixel *cur = picPixels;
    int curRow = 1;
    while(curRow < r1) {
        //position cursor in right row
        cur = cur + *columns;
        curRow++;
    }
    //position cursor in right column
    cur = cur + c1 - 1;
    printf("Cropping region from (%d,%d) to (%d,%d)...\n",c1,r1,c2,r2);
    //copy pixels until end crop position is reached
    int copiedPixels = 0;
    for(int r = 0; r < rowCrop; r++) {
        for(int c = 0; c < colCrop; c++) {
            //copy appropriate pixels in given line
            (croppedPix + copiedPixels)->red = (cur + c)->red;
            (croppedPix + copiedPixels)->green = (cur + c)->green;
            (croppedPix + copiedPixels)->blue = (cur + c)->blue;
            copiedPixels++;
        }
        //reposition cursor to beginning of next line
        cur = cur + *columns;
    }
    //update external value of rows and columns for correct writing of picture to file
    *columns = colCrop;
    *rows = rowCrop;
    //free picPixels to use same variable name in program with the new cropped pixels.
    free(picPixels);
    return croppedPix;
}

void invert(pixel *picPixels, int *columns, int *rows) {
//Inverts the color pixels of an image.
    printf("Inverting intensity...\n");
    int numPixels = (*columns)*(*rows);
    for(int changedPixels = 0; changedPixels < numPixels; changedPixels++) {
        (picPixels + changedPixels)->red = 255 - (picPixels + changedPixels)->red;
        (picPixels + changedPixels)->green = 255 - (picPixels + changedPixels)->green;
        (picPixels + changedPixels)->blue = 255 - (picPixels + changedPixels)->blue;
    }

}

void swap(pixel *picPixels, int *columns, int *rows) {
//Swaps color channels of picture.
    printf("Swapping color channels...\n");
    int numPixels = (*columns)*(*rows);
    char temp;
    for(int changedPixels = 0; changedPixels < numPixels; changedPixels++) {
        temp = (picPixels + changedPixels)->red;
        (picPixels + changedPixels)->red = (picPixels + changedPixels)->green;
        (picPixels + changedPixels)->green = (picPixels + changedPixels)->blue;
        (picPixels + changedPixels)->blue = temp;
    }
}

void grayscale(pixel *picPixels, int *columns, int *rows) {
//Converts pixels in image to grayscale according to the NTSC standard conversion
//formula
    printf("Converting to grayscale...\n");
    int numPixels = (*columns)*(*rows);
    int intensity;
    for(int changedPixels = 0; changedPixels < numPixels; changedPixels++) {
        intensity = 0.30*(picPixels + changedPixels)->red + 0.59 * (picPixels + changedPixels)->green + 0.11*(picPixels + changedPixels)->blue;
        (picPixels + changedPixels)->red = intensity;
        (picPixels + changedPixels)->green = intensity;
        (picPixels + changedPixels)->blue = intensity;
    }
}

unsigned char saturate(double product) {
//function guaratees brightess operatoins are done according to saturating math
    if(product > 255) {
        return (unsigned char) 255;
    } else if (product < 0) {
        return (unsigned char) 0;
    } else {
        return (unsigned char) product;
    }
}

void brightness(pixel *picPixels, int *columns, int *rows, double delta) {
//Adjusts image brightness by a given delta.
    printf("Adjusting brightness by %f...\n",delta);
    int numPixels = (*columns)*(*rows);
    for(int changedPixels = 0; changedPixels < numPixels; changedPixels++) {
        //obtain new color for red channel from delta
        double new_red = delta * (picPixels + changedPixels)->red;
        //use saturate function to assign this new color to the channel according
        //to saturating math
        (picPixels + changedPixels)->red = saturate(new_red);
        //repeat process for other two colors
        double new_green = delta * (picPixels + changedPixels)->green;
        (picPixels + changedPixels)->green = saturate(new_green);
        double new_blue = delta * (picPixels + changedPixels)->blue;
        (picPixels + changedPixels)->blue = saturate(new_blue);
    }
}
