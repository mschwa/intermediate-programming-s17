/* File: hw1.c
 * Homework 1, 600.120 Spring 2017
 *
 * Marcos Schwartz, mschwa69
 */

#include <stdio.h>
#include <string.h>

const int LENGTH = 16;

int main(int argc, char* argv[]) {
    //define strings containing the number correspondence of codes (only last letter
    //for division code is necessary since that letter is unique)
    char div_letter[8] = {'E','U','D','N','S','H','Y','A'}; 
    char grade[8] = {'A','B','C','D','F','I','S','U'};
    char sign[3] = {'+','-','/'};

    // Confirm that a command-line argument is present
    if (argc == 1) {
        printf("Usage: hw1 XX.###.###Gg#.#\n");
	return 1;
	// exit program because no command line argument is present
    }

    //Declare a char array to hold the command-line argument string;
    char course[LENGTH];
    //Copy at most LENGTH characters of the input argv[1] into the characater array course
    strncpy(course,argv[1],LENGTH);
    //Add null character so that course array can be printed
    course[LENGTH-1] = '\0';

    //Printing division number
    char div = course[1];
    int i = 0;
    while(div != div_letter[i]){
        i += 1; 
    }
    printf("Division: %d\n",i);

    //printing the deparment
    printf("Department: ");
    for (i=3;i<6;i++){
        putchar(course[i]);
    }

    //printing course number  
    printf("\nCourse :");
    for (i=7;i<10;i++){
        putchar(course[i]);
    }

    //Printing grade:
    char letter = course[10];
    i = 0;
    while(letter != grade[i]){
        i += 1; 
    }
    printf("\nGrade: %d",i);

    i = 0;
    char symb = course[11];
    while(symb != sign[i]){
        i += 1; 
    }
    printf(" %d\n",i);

    //Printing credits:
    if (course[14]=='0'){
        printf("Credits: %c 0\n",course[12]);
    }
    else {
        printf("Credits: %c 1\n",course[12]);
    }
}


