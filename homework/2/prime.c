#include <stdio.h>

int main(){
    for (int n=2;n<100;n++) {
        int p=1;
        for (int d=2; p&&(d<n);d++){
            if(n%d==0){
                p=0;
            }
        }
        if(p) {
            printf("%d ",n);
        }
    }
}
