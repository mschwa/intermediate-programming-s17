/* File: dnaseach.c
 * Homework 4, 600.120, Spring 2017
 *
 * Marcos Schwartz, mschwa69
 * mschwa69
 * 04/04/17
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int checkInput(FILE* input) {
//Function checks validity of a text file. If text file is valid, its content is
//saved in an intermediary file called "processInput" and the function returns
//number of characters in it. Otherwise, it returns -1.
    //define valid base characters
    char bases[4] = {'A','C','G','T'};
    //define variables that will store relevant values
    char c = (char)fgetc(input);
    int charCount = 0;

    //Write c to output file.
    FILE* processInput = fopen("processInput","w");

    while (c != EOF) {
        //Make c uppercase letter for easier comparison
        c = (char)toupper(c);
        int match = 0;
        for (int i=0; i<4; i++) {
            if (c == bases[i]) {
                //if character being read matches one of the bases characters,
                //we indicate this to the program by updating the value of match
                match = 1;
            }
        }
        if (match) {
            fprintf(processInput,"%c",c);
            charCount++;
        } else {
            if (isspace(c)) {
                //simply ignore c if it is some sort of space
            }
            else {
                printf("Invalid text file\n");
                //remove processed text file in case of invalid input
                remove("processInput");
                //close input text file
                fclose(input);
                return -1;
            }
        }
        c = (char)fgetc(input);
    }
    if (charCount > 15000 || charCount==0) {
        //if file contains more than 1500 valid basis
        printf("Invalid text file \n");
        remove("processInput");
        //close input text file
        fclose(input);
        return -1;
    }
    //close generated text file
    fclose(processInput);
    return charCount;
}

unsigned int* findPatt(char pattern[],char* text, int textLength) {
//Function finds provided pattern in a text file and the prints out the position of the
//matches.
    //declare variable that will store position in the array
    unsigned int position = 0;
    unsigned int* offsets = malloc(15000*sizeof(unsigned int));
    int numOffsets = 0; //stores the number of matches (= num offsets)
    while (position < textLength) {
        //if program comes across a character that matches the first one of the pattern
        if (toupper(text[position]) == toupper(pattern[0])) {
            unsigned int match = 1;
            for (unsigned int j = 1; j<strlen(pattern); j++) {
                //check if all characters following it conform to the pattern
                if(toupper(pattern[j]) != toupper(text[position+j])) {
                    match = 0;
                }
            }
            //if pattern is found in the text file
            if (match) {
                printf(" %d",position);
                offsets[numOffsets] = position;
                match=0; //reset the value of match
                numOffsets++;
            }
        }
        position++;
    }
    offsets = (unsigned int*)realloc(offsets,numOffsets*(sizeof(unsigned int)));
    //null terminate array of chars:
    if (numOffsets ==0) {
        printf(" Not found");
    }
    printf("\n");
    return offsets;
}

int getPatt(char* text,int textLength) {
//function gets patterns typed by user and checks their validity.
//It calls findPAtt to return analysis of valid patterns
    //declare array that will store the characters input by user
    //giving enough space for excessively long input to be identified.
    char pattern[15001];
    while (scanf("%15000s",pattern)!=EOF) {
        //while the user is providing patterns. Scanf reads input patterns containing
        //up to 15000 characters.
        int i = 0;
        char upper;
        while (pattern[i] != '\0') {
            //check collected pattern character-by-character
            upper = toupper(pattern[i]);
            //store uppercase version of read character in variable for validity test
            if ((upper != 'A') && (upper != 'C') && (upper != 'G') && (upper != 'T')) {
                printf("Invalid pattern\n");
                return 1;
            }
            i++;
        }

        if (strlen(pattern)>textLength) {
            printf("Invalid pattern\n");
            return 1;
        }
        //if input is valid, print uppercase pattern and offsets
        for(unsigned int j = 0; j<strlen(pattern); j++) {
            printf("%c",toupper(pattern[j]));
        }
        findPatt(pattern,text,textLength);
    }
    return 0;
}
