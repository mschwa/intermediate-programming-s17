#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>

void main(){
    printf("isdigit('8'): %d\n", isdigit('8'));
    printf("isdigit('$')): %d\n", isdigit('$'));
    printf("isdigit('A')): %d\n", isdigit('A'));
    printf("isdigit('0')): %d\n", isdigit('0'));
}
