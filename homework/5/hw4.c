/* File: hw4.c
 * Homework 4, 600.120, Spring 17
 *
 * Marcos Schwartz, mschwa69
 * 03/05/17
 */

#include "dnasearch.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char* argv[]) {
    //Call checkInput function to test validity of provided input file
    FILE* input = fopen(argv[1],"r");
    //check validity of input file. If it's valid, get the number of characters in it
    int numChars = checkInput(input);
    if (numChars == -1) {
        return 1;
    }
    //create array to store text in input file
    char* text = malloc(((unsigned int)numChars+1)*sizeof(char));
    //null terminate it
    text[numChars] = '\0';
    //open intermediaty file created by checkInput to store input text to the
    //array defined above
    FILE* processInput = fopen("processInput","r");
    fscanf(processInput,"%s",text);
    //prompt user for patterns
    getPatt(text,(unsigned int)strlen(text));
    //remove intermediary file and close text file.
    remove("processInput");
    free(text);
    return 0;
}
