/*
 * File: catalog.h
 * Content: declaration of functions in catalog.c
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#ifndef CATALOG_H
#define CATALOG_H

#include <stdio.h>
#include <stdlib.h>

// Define struct where information about course will be stored
typedef struct GeneralCourseInfo {
    char listing[11];
    char div[3]; //space for null terminator
    char dpt[4];
    char num[4];
    float cred;
    char title[33]; //space for null terminator
} genCourse; 

//Define struct where student information regarding a course will be stored.
typedef struct StudentCourseInfo {
    genCourse *course; //pointer so that updates on catalog are reflected here
    char semester[7]; //text representation of semester in yyyy.s format
    char grade[3]; //text representation of grade in Gg format
    struct StudentCourseInfo *next; //recursive definition of pointer for linked list   
} stuCourse; 

char** checkListing(char* listing);
//checks validity of course listing. If listing is valid, returns array pointing to it

int isfloat(char *string);
//checks if string is in c.f format

genCourse* validateCatalog(FILE* input, int* courseCount);
//validates input catalog file and creates course array if valid catalog is given.

void printCourse(genCourse* catalog);
//prints complete course information to screen.

void dispCat(genCourse* catalog, int* courseCount);
//1: prints catalog array.

int matchCatalog(char* courseID, genCourse* catalog, int* courseCount);
//matches provided listing (courseID) to course in catalog array

int newInput(char* curInput, genCourse* catalog, int* courseCount);
//indicates whehter it is necessary to ask user for new valid course listing.

void dispInfo(genCourse* catalog, int* courseCount);
//2: prints information about a specific course to screen.

genCourse* promptInput(genCourse* catalog, int* courseCount);
//prompts user to provide course listing while invalid input is entered. Then, returns
//position of matching course in catalog.

void updateTitle(genCourse* catalog, int* courseCount);
//3: updates the title of a course specified by user.

void updateCred(genCourse* catalog, int* courseCount);
//4: updates the credit assignmetn of a course specified by user.

int checkSemester(char *str);
//checks if string is in yyyy.s format

int checkGrade(char *str);
//checks if string is in Gg format

void addCourse(stuCourse **transcript, genCourse* course, char* semester, char* grade);
//Efectively adds valid course to transcript linked list

char* collectSemester();
//collects semester string from user

void course2transcript(genCourse *catalog, int *courseCount, stuCourse** transcript);
//5: responds to "add course to transcript" command

int findTrans(stuCourse* transcript, char *listing);
//finds number of occurrences of a course on the transcript

void pop(stuCourse **transcript, char *listing, char *semester);
//removes struct from transcript linked list

void deleteCourse(stuCourse **transcript, genCourse *catalog, int *courseCount);
//6: called to carry out process of course deletion from transcript

void displayTrans(stuCourse *transcript);
//7: displays all courses in Transcript in adequal format.

void printCourseTrans(stuCourse *transcript, char *listing);
//prints semester and grade of specified course;

void specTrans(stuCourse *transcript, genCourse *catalog, int *courseCount);
//8: prompts user to provide listing of course to be printed

double letter2grade(char *grade);
//converts valid letter grade from Gg format to double

void GPA(stuCourse *transcript);
//9: calcualtes students overall GPA
#endif
