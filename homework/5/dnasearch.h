/* File: dnasearch.h
 * Homework 4, 600.120, Spring 2017
 *
 * Marcos Schwartz
 * mschwa69
 * 04/04/17
 */

#include <stdio.h>
#include <stdlib.h>

int checkInput(FILE* input);
//Function checks validity of input text file. If text file is valid, its content
//is saved in an intermediary file called "proccessInput" and the function returns
//the number of characters in it. Otherwise, function returns -1.

unsigned int* findPatt(char pattern[], char* text, unsigned int textLenght);
//Function finds a given pattern in a text file and prints out the position of
//matches and returns the number of matches.

int getPatt(char* text, unsigned int textLength);
//Function gets patterns typed by user and checks their validity. It calls 
//findPatt to return analysis of valid patterns.
