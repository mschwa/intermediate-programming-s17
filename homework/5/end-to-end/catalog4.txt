EN.550.171 4.0 Discrete Mathematics
AS.220.200 3.0 Introduction to Fiction
EN.600.104 1.0 Computer Ethics
EN.600.107 3.0 Intro. Programming in Java
EN.600.120 4.0 Intermediate Programming
AS.280.350 3.0 Fundamentals of Epidamiology
EN.600.226 4.0 Data Structures
EN.600.233 3.0 Computer System Fundamentals
EN.600.250 3.0 User Interf. & Mobile Apps
EN.600.271 3.0 Automata & Computation Theory
AS.360.625 0.0 Responsible Conduct of Reasearch
EN.600.316 3.0 Database Systems
EN.600.321 3.0 Object Oriented Software Engr
AS.250.205 3.0 Introduction to Computing
EN.600.328 3.0 Compilers and Interpreters
EN.600.363 3.0 Introduction to Algorithms
AS.384.215 4.0 Second Year Hebrew
EN.600.426 3.0 Princ. of Prog. Languages
EN.600.491 1.5 Computer Science Workshop I
EN.661.110 3.0 Prof. Comm. for Science
AS.110.202 4.0 Calculus III
AS.180.301 4.5 Microeconomic Theory
EN.530.420 4.0 Robot Sensors & Actuators
AS.060.113 3.0 Expository Writing
EN.530.334 3.0 Heat Transfer
EN.660.203 3.0 Financial Accounting
EN.530.421 3.0 Mechatronics
AS.180.367 3.0 Investments
AS.010.101 4.0 Intro to History West Art
AS.030.507 3.0 Indep. Research in Biochem II
EN.530.495 4.0 Microfabrication Laboratory
BU.061.506 9.5 Fabricated Course
AS.100.117 3.0 History of Brazil
EN.540.409 4.0 Dynamic Modeling and Control
AS.110.798 0.0 Seminar in Number Theory
AS.150.219 3.0 Intro to Bioethics
EN.553.420 4.0 Intro to Probability
AS.171.201 4.0 Special Relativity/Waves
AS.180.605 0.0 Advanced Macroeconomics
AS.191.335 3.0 Arab-Israeli Conflict (IR)
AS.200.306 3.0 Psychology in the Workplace
EN.530.632 3.0 Convection 
AS.210.312 3.0 Advanced Spanish
AS.215.462 3.0 Junot Diaz and Oscar Wilde
AS.230.501 3.0 Research Assistanship
