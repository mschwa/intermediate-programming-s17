/* Homework 5, 600.120, Spring 2017
 * File: unittest.c
 * Content: unit tests of functions defined in catalog.c
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#include "catalog.h"
#include "prompts.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void test_validateCatalog(int* courseCount) {
//Checks if validateCatalog function from catalog.c properly filters input catalogs.
    FILE *test; //will hold handle of catalog being tested
    genCourse* result; //will point to catalog array generated

    //Test 1: provided catalog1.txt
    test = fopen("TextFiles/catalog1.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result); //confirm array is populated
    //check if it is correctly populated
    assert(strcmp(result->div,"EN") == 0);
    assert(strcmp(result->title,"Discrete Mathematics")==0);
    assert((result+14)->cred == 3.0);
    assert(atoi((result+14)->num) == 110);
    //terminate Test 1
    free(result);
    fclose(test);

    //Test2: provide valid file with more than 20 courses
    *courseCount = 0;
    test = fopen("TextFiles/catalog2.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result); //confirm array is populated
    //check if it is correctly populated
    assert(strcmp(result->div,"EN") == 0);
    assert(strcmp(result->title,"Discrete Mathematics")==0);
    assert((result+22)->cred == 3.0);
    assert(strcmp((result+22)->title,"Investments")==0);
    assert(atoi((result+22)->num) == 367);
    //terminate Test 2
    free(result);
    fclose(test);

    //Test 3: provide valid file containing a course whose credit assignment has
    //more than 2 digits
    *courseCount = 0;
    test = fopen("TextFiles/catalog3.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result); //confirm array is populated
    //check if it is correctly populated
    assert(strcmp(result->div,"EN") == 0);
    assert(strcmp(result->title,"Discrete Mathematics")==0);
    assert((int)(1000*(result+16)->cred) == 14587);
    assert(strcmp((result+16)->title,"Microeconomic Theory")==0);
    assert(atoi((result+16)->num) == 301);
    //terminate Test 3
    free(result);
    fclose(test);

    //Test 4: provide file contained one integer credit assignment
    *courseCount = 0;
    test = fopen("TextFiles/catalog4.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result == NULL); //confirm array isn't populated
    fclose(test);

    //Test 5: provide file with last course having an excessively long title
    *courseCount = 0;
    test = fopen("TextFiles/catalog5.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result == NULL); //confirm array isn't populated
    fclose(test);

    //Test 6: provide file with division having more than two characters.
    *courseCount = 0;
    test = fopen("TextFiles/catalog6.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result == NULL); //confirm array isn't populated
    fclose(test);

    //Test 7: provide file with department having more than three digits.
    *courseCount = 0;
    test = fopen("TextFiles/catalog7.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result == NULL); //confirm array isn't populated
    fclose(test);

    //Test 8: provide file with course number having more than three digits.
    *courseCount = 0;
    test = fopen("TextFiles/catalog8.txt","r");
    result = validateCatalog(test,courseCount);
    assert(result == NULL); //confirm array isn't populated
    fclose(test);
}

void test_isfloat() {
    assert(isfloat("3.14"));
    assert(isfloat("13454312.122345"));
    assert(isfloat("13454312.1223.5")==0);
    assert(isfloat(".13454312122345"));
    assert(isfloat("13454312122345.")==0);
    assert(isfloat("1345as43121223.45")==0);
}

void test_checkListing() {
    //TEST 1: invalid course with same number of characters as valid course
    char **result1 = checkListing("EN.1000.01");
    assert(result1 == NULL);
    free(result1);

    //TEST 2: valid course listing
    char **result2 = checkListing("eN.100.001");
    assert(result2);
    free(result2);

    //TEST 3: valid course listing
    char **result3 = checkListing("XX.001.999");
    assert(result3);
    free(result3);

    //TEST 4: valid listing (according to assignment specifications)
    char **result4 = checkListing("4o.001.001");
    assert(result4);
    free(result4);

    //TEST 5: invalid course
    char **result5 = checkListing("oo.0$0.001");
    assert(result5 == NULL);
    free(result5);

}

void test_checkSemester() {
    assert(checkSemester("1997.F"));
    assert(checkSemester("8004.S"));
    assert(checkSemester("0001.F"));
    assert(!checkSemester("4a53.F"));
    assert(!checkSemester("19997.F"));
    assert(!checkSemester("0001.U"));
}

void test_checkGrade() {
    assert(checkGrade("a+"));
    assert(!checkGrade("AA+"));
    assert(checkGrade("U/"));
    assert(!checkGrade("E-"));
    assert(!checkGrade("AF"));
    assert(!checkGrade("F-"));
}

void test_letter2grade() {
    assert(letter2grade("A+")== 4.0);
    assert(letter2grade("A/")== 4.0);
    assert(letter2grade("B-")== 2.7);
    assert(letter2grade("E/")== 0.0);
    assert(letter2grade("D+")== 1.3);
}

void test_matchCatalog(genCourse *catalog, int *courseCount) {
    //TEST 1: course in catalog
    assert(matchCatalog("EN.550.171",catalog,courseCount) >= 0);
    //TEST 2: course not in catalog
    assert(matchCatalog("EN.551.171",catalog,courseCount) == -1);
}

void test_addCourse(stuCourse **transcript) {
    //Define course to be added at 3 different positions. Same course will be added,
    //but with different semesters. Courses passed to this function are filtered before
    //it is called, so it only has to handle valid courses
    genCourse *course3 = malloc(sizeof(genCourse));
    strcpy(course3->listing,"EN.600.226");
    strcpy(course3->div,"EN");
    strcpy(course3->dpt,"600");
    strcpy(course3->num,"226");
    course3->cred = 4.0;
    strcpy(course3->title,"Data Structures");
    //TEST 1: insert course at first position
    addCourse(transcript,course3,"2015.F","D+");
    assert(strcmp((*transcript)->course->title,"Data Structures") == 0);


    free(course3);
}

int main() {
    int *courseCount = malloc(sizeof(int));
    test_validateCatalog(courseCount);

    test_checkListing();

    test_isfloat();
    test_checkSemester();
    test_checkGrade();
    test_letter2grade();


    //create catalog for testing:
    *courseCount = 0;
    FILE *cat = fopen("TextFiles/catalog1.txt","r");
    genCourse* catalog = validateCatalog(cat,courseCount);
    fclose(cat);

    printf("Program passed all tests!\n");
    free(catalog);
    free(courseCount);
}


