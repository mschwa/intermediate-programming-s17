/* Homework 5, 600.120, Spring 2017
 * File: hw5.c
 * Content: main function that initiates the program.
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#include "catalog.h"
#include "prompts.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]) {
    if (argc == 1) {
        printf("Provide catalog text file.\n");
        return -1;
    }

    //intialize variable the will hold the number of courses in catalog
    int *courseCount = malloc(sizeof(int));
    *courseCount = 0;

    //Open and validate input file. Create catalog array if it is valid.
    FILE* input = fopen(argv[1],"r");
    genCourse *catalog = validateCatalog(input, courseCount);
    if (catalog == NULL) {
        return -1;
    }

    //initialize transcript linked list
    stuCourse *transcript = NULL;

    //Start interaction
    int run = 1;
    char command;
    while (run) {
        if (command != '\n' && command != EOF) {
            //don't print menu while discarning newline character in buffer
            menu_prompt();
        }
        command = getchar();
        switch(command) {
        case 'q':
            run = 0;
            break;
        case '1':
            dispCat(catalog, courseCount);
            break;
        case '2':
            dispInfo(catalog, courseCount);
            break;
        case '3':
            updateTitle(catalog, courseCount);
            break;
        case '4':
            updateCred(catalog, courseCount);
            break;
        case '5':
            course2transcript(catalog, courseCount,&transcript);
            break;
        case '6':
            if (transcript != NULL) {
                deleteCourse(&transcript,catalog,courseCount);
            }
            break;
        case '7':
            displayTrans(transcript);
            break;
        case '8':
            if (transcript != NULL) {
                specTrans(transcript,catalog,courseCount);
            }
            break;
        case '9':
            GPA(transcript);
            break;
        case '\n':
        case EOF:
            break;
        }
    }
    //free catalog array
    free(catalog);
    //free courseCount
    free(courseCount);
    fclose(input);
    //free linked list
    if(transcript != NULL) {
        int delete = 1;
        stuCourse *Ptr1 = transcript->next;
        stuCourse *Ptr2 = transcript;
        while(delete) {
            free(Ptr2);
            if(Ptr1 == NULL) {
                delete = 0;
            } else {
                Ptr2 = Ptr1;
                Ptr1 = Ptr1->next;
            }
        }
    }
    return 0;
}
