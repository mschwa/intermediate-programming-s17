/* Homework 5, 600.120, Spring 2017
 * File: catalog.c
 * Content: definition of structures and functions used for course cataloguing and
 *          transcript representation.
 *
 * Marcos Schwartz, mschwa69
 * 03/26/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "catalog.h" //for struct declarations
#include "prompts.h"

char** checkListing(char* listing) {
//Checks validity of course listing. If it is valid, returns  pointer to array
//containing course division, department and number. Otherwise, returns NULL pointer
    int valid = 1;
    //Copy course listing to use strtok and validate course division, department,
    //and number.
    char listingCopy[strlen(listing)];
    strcpy(listingCopy,listing);
    //Division must be a two-character string
    char* division = strtok(listingCopy,".");
    if (strlen(division) != 2) {
        valid = 0;
    }
    //Department and course number must be three-digit numbers:
    char *department = strtok(NULL,".");
    char *number = strtok(NULL,".");
    if (strlen(department) != 3 || strlen(number) != 3){
        valid = 0;
    }
    else {
        for(int i=0; i<3; i++)
            if(!isdigit(department[i]) || !isdigit(number[i])){
                valid = 0;
            }
    }
    if (valid) {
        char **ID = malloc(3*sizeof(char *));
        ID[0] = malloc(sizeof(division));
        ID[1] = malloc(sizeof(department));
        ID[2] = malloc(sizeof(number));
        strcpy(ID[0], division);
        strcpy(ID[1], department);
        strcpy(ID[2], number);
        return ID;
    } else {
        return NULL;
    }
}

int isfloat(char *string) {
//checks if string is in c.f format. Returns 1 if its is, 0 otherwise.
    int dotCount = 0;
    int nondigitCount = 0;
    int len = strlen(string);
    for (int i = 0; i < len; i++) {
        if (string[i] == '.') {
            //keep track of all '.' characters in string
            dotCount++;
        }
        if (!isdigit(string[i])) {
            //check if there's any invalid character in string
            nondigitCount++;
        }
    }
    if ((dotCount == 1) && (nondigitCount == 1) && (string[len-1]!='.')) {
        //if there's only one dot and no numbers in string, it is in the c.f format
        return 1;
    } else {
        return 0;
    }
}

genCourse* validateCatalog(FILE* input, int* courseCount) {
//Validates input catalog files. If valid catalog is provided, function returns
//catalogue array. Otherwise, it retuns a NULL value.
    unsigned int valid = 1;
    char line[1001];
    char lineCopy[1001]; //copy of line used by strtok
    int size = 20;
    genCourse *catalog = malloc(size*sizeof(genCourse));
    //check if file is empty
    fseek(input,0,SEEK_END);
    int filesize = ftell(input);
    if (filesize < 2){
        return catalog;
    }
    rewind(input);
    
    while ( fgets(line,1000,input) != NULL) {
        if (line != NULL) {
            // Remove newline character of read line:
            line[strlen(line)-1] = '\0';
        } else {
            valid = 0;
        }
        // Make copy of line that can be modified by strtok function
        strcpy(lineCopy,line);
        // Use strtok to read course listing (fist token)
        char *listing = strtok(lineCopy," ");
        //Check validity of credit assignment
        char *credits = strtok(NULL, " ");
        if (!isfloat(credits)) {
            //if credit assignment isn't in c.f format
            valid = 0;
        }
        //use strcpy to collect the course title. Copy starts at position 1 +
        //strlen(credits) + 1,that is, one character after the end of c.f
        char title[34];
        title[33]='\0';
        strncpy(title,(line+11+strlen(credits)+1),33);
        char division[3];
        char department[4];
        char number[4];
        if (strlen(title) == 33) {
            //if title contains more than 32 characters
            valid = 0;
        } else {
            //call checklisting to validate course listing information
            char **listingInfo = checkListing(listing);
            if (listingInfo == NULL) {
                valid = 0;
            } else {
                //if listing is valid, use its values
                strcpy(division,listingInfo[0]);
                strcpy(department,listingInfo[1]);
                strcpy(number,listingInfo[2]);
                //listingInfo is only populated if listing is valid, so free
                //must be done at this block
                for (int i = 0; i<3; i++) {
                    free(listingInfo[i]);
                }
            }
            free(listingInfo);
        }
        if (valid) {
            //if input file contains more than 20 courses, update size of catalog
            if (*courseCount == size) {
                size *= 2;
                catalog = (genCourse*)realloc(catalog,size*sizeof(genCourse));
            }
            //use information of valid course to populate the catalog array
            strcpy((catalog+*courseCount)->listing,listing);
            strcpy((catalog+*courseCount)->div, division);
            strcpy((catalog+*courseCount)->dpt, department);
            strcpy((catalog+*courseCount)->num, number);
            (catalog+*courseCount)->cred = atof(credits);
            strcpy((catalog+*courseCount)->title, title);
        } else {
            //if input file is invalid, free catalog and end validation
            free(catalog);
            return NULL;
        }
        //update couting of courses
        (*courseCount)++;
    }
    //adjust size of catalog according to number of courses provided
    catalog = (genCourse*)realloc(catalog,(*courseCount)*sizeof(genCourse));
    return catalog;
}

void printCourse(genCourse* catalog) {
//Prints complete course information to screen in the xx.ddd.nnn c.f Course Title\n
//format.
    printf("%s",catalog->div);
    printf(".%s.",catalog->dpt);
    printf("%s ",catalog->num);
    printf("%.1f ",catalog->cred);
    printf("%s\n",catalog->title);
}

void dispCat(genCourse* catalog, int* courseCount) {
//Prints complete information on all courses available. When called from the interactive
// menu, file argument will be equal to NULL, so that catalog is printed to screen.
    if (courseCount) {
        for (int i=0; i < *courseCount; i++) {
            printCourse(catalog+i);
        }
    }
}

int matchCatalog(char* courseID, genCourse* catalog, int* courseCount) {
//Matches course identifier to course in catalogue. If match exists, returns the
//position of the course in the catalog. Otherwise, returns -1.
    // Declare char array that will store the concatenation of course
    // information to provide listing of course read in the catalog
    char curListing[11];
    int i;
    for(i=0 ; i < *courseCount; i++) {
        //form listing of course being currently read
        strcpy(curListing,(catalog+i)->listing);
        //if course provided matches one of the courses in the catalog
        if(strcmp(curListing,courseID) == 0) {
            return i;
        }
    }
    return -1;
}

int newInput(char* curInput, genCourse* catalog, int* courseCount) {
//Function assesses the necessity of asking user to input new course listing: if -2 is
//returned, user provided invalid listing; if -1 is returned, listing is valid but
//finds no match in the catalog. Otherwise, position of matching course in the catalog
//is returned.
    if (strlen(curInput) == 10) {
        //if provided listing has appropriate length, proceed to check
        //the information embeded in it
        char **info = checkListing(curInput);
        if (info) {
            //if input passes checkListing test (is a valid listing),first make it
            //case insenstive.
            *curInput = (char)toupper(*curInput);
            *(curInput+1) = (char)toupper(*(curInput+1));
            //Then, check if it matches any course in the catalog
            int matchPos = matchCatalog(curInput,catalog,courseCount);
            //free array created from checkListing
            for (int i=0; i<3; i++) {
                free(info[i]);
            }
            free(info);
            return matchPos;
        } else {
            free(info);
            return -2;
        }
    } else {
        return -2;
    }
}

genCourse* promptInput(genCourse* catalog, int* courseCount) {
//prompts user for input until valid course listing is provided. Returns position of
//matching course in array.
    int invalid = 1;
    char courseID[12]; //extra space to check validity of input
    int inputCheck;
    char c; //variable used for clearing stdin buffer
    while(invalid) {
        course_prompt();
        scanf("%11s",courseID); //collect listing from user
        //clear buffer after using scanf
        while(((c = getchar())!= '\n')&&(c != EOF)) {}
        //check validity of provided input
        inputCheck = newInput(courseID, catalog, courseCount);
        if (inputCheck == -2) {
            invalid_input_msg();
        } else if (inputCheck == -1) {
            course_absent_msg();
        } else {
            invalid = 0;
        }
    }
    return (catalog+inputCheck);
}

void dispInfo(genCourse* catalog, int* courseCount) {
//Displays information on a specific course in the catalog.
    //get address of course whose information is being required by the user
    genCourse *match = promptInput(catalog, courseCount);
    //print course
    printCourse(match);
}

void updateTitle(genCourse *catalog, int* courseCount) {
//Updates the title of a specific course
    //Prompt user to provide course whose title they want to update and get position
    //of course in catalog.
    genCourse *match = promptInput(catalog, courseCount);
    //get new title from user
    char newTitle[33];
    int invalid = 1; //control variable of while loop
    unsigned int inputLen = 0; //keeps track of number of chars in provided title
    char inChar;
    int restart = 0; //indicates whether it is necessary to ask user for new title
    while (invalid) {
        if (restart) {
            inChar = '0'; //assigns valid to inChar that makes it enter while loop
            restart = 0;
        }
        new_title_prompt();
        while((inChar != '\n') && (inChar != EOF)) {
            inChar = getchar();
            if (inputLen < 33 && (inChar!= '\n') && (inChar != EOF)) {
                //build newTitle string, ignoring newline characters
                *(newTitle + inputLen) = inChar;
            }
            inputLen++;
        }
        newTitle[inputLen-1] = '\0'; //null terminate new title at appropriate position
        if (inputLen < 33) {
            //if input has valid length
            invalid = 0;
        } else {
            invalid_input_msg();
            //reset variables used for title collection
            inputLen = 0;
            restart = 1;
        }
    }
    //update title once valid string is provided
    strcpy(match->title,newTitle);
    course_updated_msg();
}

void updateCred(genCourse *catalog, int* courseCount) {
//Updates the credit assignment of a specific course
    //get new number of credits from user
    float newCred;
    char newCred_str[21];
    int invalid = 1;
    unsigned int inputLen = 0; //keeps track of number of chars in provided title
    char inChar;
    int restart = 0; //indicates whether it is necessary to ask user for new title
    //prompt user to provide course whose credit assignment they want to update and
    //get position of course in catalog.
    genCourse *match = promptInput(catalog, courseCount);
    while (invalid) {
        if (restart) {
            inChar = '0'; //assigns valid to inChar that makes it enter while loop
            restart = 0;
        }
        new_credit_prompt();
        while((inChar != '\n') && (inChar != EOF)) {
            inChar = getchar();
            if ((inputLen < 21) && (inChar!= '\n') && (inChar != EOF)) {
                //build newCred_str, ignoring newline characters
                *(newCred_str + inputLen) = inChar;
            }
            inputLen++;
        }
        newCred_str[inputLen-1] = '\0'; //null terminate string at appropriate position
        if(isfloat(newCred_str)) {
            //if isfloat function confirms input is in c.f format
            invalid = 0;
            newCred = atof(newCred_str);
        } else {
            invalid_input_msg();
            inputLen = 0;
            restart = 1;
        }
    }
    match->cred = newCred;
    course_updated_msg();
}

int checkSemester(char *str) {
//Checks if provided str is in yyyy.s format. Returns 1 if it is, 0 otherwise.
    int i = 0;
    int valid = 1;
    if(strlen(str) != 6) {
        //if string doesn't contain 6 characters, it cannot be valid
        valid = 0;
    } else {
        while(i < 4) {
            //check if first four characters are numbers
            if(isdigit(*(str+i)) == 0) {
                valid = 0;
            }
            i++;
        }
        if(*(str+4) != '.') {
            //check if 5th character is a dot
            valid = 0;
        }
        if(*(str+5) != 'F' && *(str+5) != 'S') {
            //check if semester letter is valid
            valid = 0;
        }
    }
    if (valid) {
        return 1;
    } else {
        return 0;
    }
}

int checkGrade(char *str) {
//Checks if provided str is in Gg format.
    const char grade[8] = {'A','B','C','D','F','I','S','U'};
    int match = 0;
    if(strlen(str) != 2) {
        //only grade string with 2 chars can be valid
        match = 0;
    } else {
        for (int i=0; i < 8; i++) {
            if ((char)toupper(*str) == *(grade+i)) {
                //if provided letter grade is valid
                match = 1;
            }
        }
        if (match) {
            if((*(str+1) != '+') && (*(str+1) != '-') &&(*(str+1) != '/')) {
                //if second character isn't a valid sign
                match = 0;
            }
        }
    }
    //finally grade letter cannot be D-, and F,I,S and U cannot be signed 
    if(*str == 'D'){
        if(*(str+1) == '-'){
            match = 0;
        }  
    } else if(*str == 'F' || *str == 'I' || *str == 'S' || *str == 'U'){
        if(*(str+1) != '/'){
            match = 0;
        }
    }

    if (match) {
        return 1;
    } else {
        return 0;
    }
}

void addCourse(stuCourse **transcript, genCourse* course, char *semester, char *grade) {
//Adds valid course to student transcript in order by semester. For same semester, most
//recent additions are added first.
    stuCourse *Ptr2 = *transcript;
    //define control variables of loops used to insert course in transcript
    int loop = 1;
    if (Ptr2==NULL) {
        //define new course struct to be inserted in list
        stuCourse *newCourse = malloc(sizeof(stuCourse));
        newCourse->course = course;
        strcpy(newCourse->semester,semester);
        strcpy(newCourse->grade,grade);
        //since list is empty, we just have to make the head point to the new element
        //and the new element point to NULL.
        *transcript = newCourse; 
        newCourse->next = NULL;

    } else {
        stuCourse *Ptr1 = (*transcript)->next;
        //define new course struct to be inserted in list
        stuCourse *newCourse = malloc(sizeof(stuCourse));
        newCourse->course = course;
        strcpy(newCourse->semester,semester);
        strcpy(newCourse->grade,grade);
        newCourse->next = NULL;
        if(strcmp(semester,Ptr2->semester) <= 0) {
            //BOUNDARY CASE: course to be inserted at first position
            newCourse->next = Ptr2;
            *transcript = newCourse;
        } else if(Ptr1 == NULL || strcmp(semester,Ptr1->semester) == 0) {
            //If fist course in the transcrip was taken in the same semester as the
            //course we want to add
            Ptr2->next = newCourse;
            newCourse->next = Ptr1;
        } else {
            while(loop && (strcmp(semester,Ptr1->semester) > 0)) {
                if (Ptr1->next == NULL) {
                    loop = 0;
                }
                Ptr2 = Ptr2->next;
                Ptr1 = Ptr1->next;
            }
            Ptr2->next = newCourse;
            newCourse->next = Ptr1;
        }
    }

}

char* collectSemester() {
//collects semester string from user
    char *semester = malloc(7*sizeof(char));
    int invalid = 1;
    unsigned int inputLen = 0; //keeps track of number of chars provided as input
    char inChar = '0';
    int restart = 0;
    while (invalid) {
        if (restart) {
            inChar = '0'; //assigns valid to inChar that makes it enter while loop
            restart = 0;
        }
        semester_prompt();
        while((inChar != '\n') && (inChar != EOF)) {
            inChar = getchar();
            if (inputLen < 7) {
                //build newCred_str, ignoring newline characters
                *(semester + inputLen) = (char)toupper(inChar);
                inputLen++;
            }
        }
        semester[inputLen-1] = '\0'; //null terminate string at appropriate position
        if(checkSemester(semester)) {
            //if checkSemester function confirms input is in yyyy.S format
            invalid = 0;
        } else {
            invalid_input_msg();
            inputLen = 0;
            restart = 1;
        }
    }
    return semester;
}


void course2transcript(genCourse *catalog, int* courseCount, stuCourse **transcript) {
//Adds course to student transcript
    char *semester;
    int invalid = 1; //indicates whether input grade is valid
    unsigned int inputLen = 0; //keeps track of number of chars provided as input
    char inChar = '0';
    int restart = 0; //indicates whether previous input was invalid
    int promptCourse = 1; //indicates whether it's necessary to collect user input
    while (promptCourse) {
        //Prompt user to provide listing of course they want to add to their
        //transcript and return pointer to this course.
        genCourse *match = promptInput(catalog, courseCount);
        //collect semester when course was taken
        semester = collectSemester();
        //move on to grade collection once valid semester is provided
        char grade[3];
        while (invalid) {
            if (restart) {
                inChar = '0'; //assigns valid to inChar that makes it enter while loop
                restart = 0;
            }
            grade_prompt();
            //collect input
            while((inChar != '\n') && (inChar != EOF)) {
                inChar = getchar();
                if (inputLen < 3) {
                    //build grade
                    *(grade + inputLen) = (char)toupper(inChar);
                    inputLen++;
                }
            }
            grade[inputLen-1] = '\0'; //null terminate string at appropriate position
            if(checkGrade(grade)) {
                //if checkGrade function confirms input is in Gg format
                invalid = 0;
            } else {
                invalid_input_msg();
                inputLen = 0;
                restart = 1;
            }
        }
        //Having both valid semester and valid grade strings, check if same course with
        //same semester already exists in transcript
        int courseMatch = 0;
        int semesterMatch = 0;
        stuCourse *transPtr = *transcript;
        while (transPtr != NULL) {
            if (strcmp(match->listing,transPtr->course->listing) == 0) {
                courseMatch = 1;
                if (strcmp(semester,transPtr->semester)==0) {
                    semesterMatch = 1;
                } else {
                    courseMatch = 0;
                }
            }
            transPtr = transPtr->next;
        }
        if (courseMatch && semesterMatch) {
            duplicate_course_msg();
            //Reset variables for input collection
            courseMatch = 0;
            semesterMatch = 0;
            invalid = 1;
            inputLen = 0;
            restart = 1;
            free(semester);
        } else {
            promptCourse = 0;
            addCourse(transcript,match,semester,grade);
            transcript_updated_msg();
            free(semester);
        }
    }
}

int findTrans(stuCourse* transcript, char *listing) {
//finds occurrences of a course in a student's transcript and return an array of pointers
//to the semester of these occurrences
    unsigned int numOccur = 0;
    stuCourse *Ptr = transcript; //Ptr points to first element
    //traverse list looking for matches
    while (Ptr != NULL) {
        if (strcmp(Ptr->course->listing,listing) == 0) {
            //if match is found, pointer in array of pointes is set to point to it.
            numOccur++;
        }
        Ptr = Ptr->next;
    }
    return numOccur;
}

void pop(stuCourse **transcript, char *listing, char *semester) {
//removes course struct from transcript linked list. IF semester string provided is NULL
//doesn't perform semester match to delete appropriate course
    stuCourse *Ptr1 = (*transcript)->next;
    stuCourse *Ptr2 = *transcript;
    //BOUNDARY CASE: first element has to be removed:
    if (strcmp(Ptr2->course->listing,listing) == 0) {
        if(semester == NULL) {
            *transcript = Ptr1;
            free(Ptr2);
        } else if (strcmp(Ptr2->semester,semester) == 0) {
            *transcript = Ptr1;
            free(Ptr2);
        }
    } else {
        if (semester == NULL) {
            //traverse list until course with same listing is found."
            while (strcmp(Ptr1->course->listing,listing) != 0) {
                Ptr1 = Ptr1->next;
                Ptr2 = Ptr2->next;
            }
            Ptr1 = Ptr1->next;
            //free deleted node before changing pointer of next
            free(Ptr2->next);
            Ptr2->next = Ptr1;
        } else {
            //if there is more than one occurrence of course in transcript, also compare
            //semester
            int listingMatch = 0;
            int semesterMatch = 0;
            while(Ptr1 != NULL){
                if(strcmp(Ptr1->course->listing,listing) == 0){
                    listingMatch = 1;
                }
                if(strcmp(Ptr1->semester,semester) ==0) {
                    semesterMatch = 1;
                }
                if(semesterMatch && listingMatch){
                    break;
                } else {
                    listingMatch = 0;
                    semesterMatch = 0;
                }
                Ptr1 = Ptr1->next;
                Ptr2 = Ptr2->next;
            }
            Ptr1 = Ptr1->next;
            free(Ptr2->next);
            Ptr2->next = Ptr1;
        }
    }
}


void deleteCourse(stuCourse** transcript, genCourse* catalog, int *courseCount) {
//sets up deletion of provided course course from transcript
    int prompt = 1;
    int numOccur = 0;
    genCourse *match;
    //Prompt user to provide listing of course they want to remove from  their
    //transcript and return pointer to this course.
    while(prompt) {
        match = promptInput(catalog, courseCount);
        //while course that exists in transcript isn't provided:
        //Find instances of course in transcript
        numOccur = findTrans(*transcript,match->listing);
        if (numOccur) {
            prompt = 0;
        } else {
            course_not_taken_msg();
        }
    }
    if (numOccur == 1) {
        //if course exists only once in transcript
        pop(transcript,match->listing,NULL);
        transcript_updated_msg();
    } else {
        char *semester;
        int invalid = 1;
        stuCourse *Ptr = *transcript;
        while(invalid){
            //collect semester when course was taken. Prompt for semester input until
            //user provide a semester in which course was taken
            semester = collectSemester();
            while(Ptr!=NULL){
                if (strcmp(semester,Ptr->semester) == 0){
                    invalid = 0;
                }
                Ptr = Ptr->next;
            }
            if (invalid){
                //if it is necessary to collect new semester input, free previous one
                free(semester);
                //reposition Ptr for another interation
                Ptr = *transcript;
            }
        }
        //delete course
        pop(transcript,match->listing,semester);
        free(semester);
        transcript_updated_msg();
    }
}

void displayTrans(stuCourse *transcript) {
//Prints all courses in student transcript
    stuCourse *Ptr = transcript;
    if(Ptr == NULL) {
        empty_transcript_msg();
    } else {
        while (Ptr != NULL) {
            printf("%s ",Ptr->semester);
            printf("%s ",Ptr->grade);
            printCourse(Ptr->course);
            Ptr = Ptr->next;
        }
    }
}

void printCourseTrans(stuCourse *transcript, char *listing) {
//prints semester and grade of a course in transcript.
    stuCourse *Ptr = transcript;
    while(Ptr != NULL) {
        if(strcmp(Ptr->course->listing,listing) == 0) {
            printf("%s ",Ptr->semester);
            printf("%s\n",Ptr->grade);
        }
        Ptr = Ptr->next;
    }
}

void specTrans(stuCourse *transcript, genCourse *catalog, int* courseCount) {
//handles command 8 - printing information about specific course in transcript
    int prompt = 1;
    int numOccur;
    while(prompt) {
        //Prompt user to provide listing of course they want to remove from  their
        //transcript and return pointer to this course.
        genCourse *match = promptInput(catalog, courseCount);
        //Find instances of course in transcript
        numOccur = findTrans(transcript,match->listing);
        if (numOccur) {
            prompt = 0;
            printCourseTrans(transcript,match->listing);
        } else {
            course_not_taken_msg();
        }
    }

}

double letter2grade(char *grade) {
//converts valid letter grade from Gg format to double
    const char letters[4] = {'A','B','C','D'};
    char letter = *grade;
    double num; //assigns initial value of 4.0 to number grade
    int calculate = 0;
    for (int i = 0; i<4; i++) {
        //check if letter grade requires any calculation
        if (letter == letters[i]) {
            calculate = 1;
        }
    }
    if (!calculate) {
        //if it doesn't return zero
        return 0.0;
    }
    switch(letter) {
    case 'A':
        num = 4.0;
        break;
    case 'B':
        num = 3.0;
        break;
    case 'C':
        num = 2.0;
        break;
    case 'D':
        num = 1.0;
        break;
    }
    if (num != 4.0 && *(grade+1) == '+') {
        //if the letter isn't A and a + sign was obtained, grade must be corrected by
        //plus 0.3
        num += 0.3;
    } else if (*(grade+1) == '-') {
        //if the sign is '-', the grade must be corrected by -0.3
        num -= 0.3;
    }
    return num;
}

void GPA(stuCourse *transcript) {
//function calculates student's GPA based on all courses in the student's transcipt
    stuCourse *Ptr = transcript;
    double GPA = 0.0;
    double grade_sum = 0.0;
    double weight_sum = 0.0;
    if (Ptr == NULL) {
        //if transcript is empty:
        gpa_msg(GPA);
    } else {
        while(Ptr != NULL) {
            char letter = *(Ptr->grade);
            if ((letter != 'I') || (letter != 'S') || (letter != 'U')) {
                //collect credit assignment of current course
                double cred = (double)Ptr->course->cred;
                //update the weighted sum of the grades:
                weight_sum += cred;
                grade_sum += cred*(letter2grade(Ptr->grade));
                Ptr = Ptr->next;
            }
        }
        GPA = grade_sum/weight_sum;
        gpa_msg(GPA);
    }
}
