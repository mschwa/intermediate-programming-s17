// program prompts the user for and reads five integers. Once all 5 numbers have been provided and stored in an array, program prints out the average value of array elements, as well as the elements greater than this average.
#include <stdio.h>

int main(){
    int nums[5];
    int i = 0;
    int sum = 0;
    float avg = 0;

    // Prompt user to provide integer inputs and stores them in array "nums" via scanf
    printf("Gimme the 5 integers: ");
    scanf("%i %i %i %i %i",&nums[0],&nums[1],&nums[2],&nums[3],&nums[4]);
   
    //compute the average of provided values
    for (i=0;i<5;i++){
        sum += nums[i];
    }
    avg = sum/5.0;

    //Print average value and elements of nums greater than it
    printf("\nAverage value: %.2f\nInputs greater than average: ",avg);   
    for (i=0;i<5;i++){
        if (nums[i] > avg){
            printf("%i ",nums[i]);
        }
    }
    printf("\n");
    
    return 0;
}
