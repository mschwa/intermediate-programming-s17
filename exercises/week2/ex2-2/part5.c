/* program prompts the user for and reads five integers. Once all 5 numbers have been 
provided and stored in an array, program prints out the average and mean  value of array
elements,as well as values in the array greater than this average. Then, program
constructs second array only with these numbers and prints them again from the new array.
*/

#include <stdio.h>
#include <stdbool.h>

int main(){
    //arrays
    int nums[5];        //stores inputs
    int great[5];       //contains inputs greater than averge
    int ordered[5];     //array will store inputs in ascending order
    //counters
    int i = 0;
    int c = 0;
    // arithmetic
    int sum = 0;
    float avg = 0;

    // Prompt user to provide integer inputs and stores them in array "nums" via scanf
    printf("Gimme the 5 integers: ");
    scanf("%i %i %i %i %i",&nums[0],&nums[1],&nums[2],&nums[3],&nums[4]);
   
    //compute the average of provided values
    for (i=0;i<5;i++){
        sum += nums[i];
    }
    avg = sum/5.0;

    //order inputs:
    int temp;
    //copy inputs to ordered array: 
    for (i=0;i<5;i++){
        ordered[i]=nums[i];
    }
    //to order an array of length l by comparing the term to its precedent and making 
    //necessary swaping, we must go through the array performing the swaping at most
    //l-1 times:
    for (c=0;c<5;c++){   
        //one transversion through array, from end to beginning.     
        for (i=4;i>0;i--){  
            if (ordered[i]<ordered[i-1]){
                temp = ordered[i-1];
                ordered[i-1] = ordered[i];
                ordered[i] = temp;
            } 
        }
    }
    printf("\nMean Value: %i\n",ordered[2]);

    //Print average value and elements of nums greater than it
    printf("Average value: %.2f\nInputs greater than average: ",avg);   
    c = 0; //counter must be reinitialized for following loop;
    for (i=0;i<5;i++){
        if (nums[i] > avg){
            great[c] = nums[i];  //build second array
            c += 1;              //keep track of position in array
            printf("%i ",nums[i]);
        }
    }
    printf("\nValues from array: "); 
    for (i=0;i<c;i++){
         printf("%i ",great[i]);
        }
    printf("\n\n");
    return 0;
}
