//600.120 Exercise 4-1 starter code

#include <stdio.h>

int main() {

    //recall that the format flag "%lu" represents an unsigned long int
    printf("sizeof(int) [signed] = %lu bytes\n", sizeof(int));
    printf("sizeof(unsigned int) = %lu bytes\n", sizeof(unsigned int));
    printf("sizeof(double) [signed] = %lu bytes\n", sizeof(double));

    //an unsigned int literal ends with the letter u
    unsigned int big = 4294967295u;

    if (big+1 > big) {
        printf("\nbigger!\n");
    } else {
        printf("\nsmaller!\n");
    }

    return 0;
}

