#include <stdio.h>
#include <string.h>

int decode(long code);
int decodeArray(long code[], int num);

int main() {
  // code1 decodes to: "secret"
  long code1 = 127979059635571;
  long code2 = 207000531811;
  long code3 = 111516585719152;
  long code4 = 7954788;
  
  // decode first riddle
  decode(code1); decode(code2); decode(code3); decode(code4);
  printf("\n");

  // code5 decodes to: "It's a secret to everyone..."
  long code5[] = { -8295737305615266890, -8031079719798727526, -7957712598822446369, -774778470 };
  long code6[] = { -2338898147514671460, -8243109524985308517, -8824082329344963617, -1063611515 };
  long code7[] = { -7599578516294035541, -7017488264183442541, -2188652 };
  long code8[] = { -7737574625590670377, -7791334147354075238, -2712938 };

  // decode seconde riddle
  decodeArray(code5,4);decodeArray(code6,4);decodeArray(code7,3);decodeArray(code8,3);
  return 0;
}
// stopped correcting the bugs to get array inputs
int decode(long code){
    int size = sizeof(code);
    for (int i=0;i<size;i++){
        printf("%c", ((char)code));
        code = code >> 8;
    }
    return 0;
}

int decodeArray(long code[], int num){
    for(int i =0; i<num;i++){
        long inverse = ~code[i];
        decode(inverse);
    }
    printf("\n");
    return 0;
}
