#include <stdio.h>
#include <stdlib.h>  
#include <assert.h>  


int* createArray(int size, int value);
void readArray(FILE *fp, int *newSize, int **newArray);
void printArray(int *array, int size);
int* my_realloc(int* ptr, int oldSize, int newSize);


int main(int argc, char* argv[]) {

  //Collect int values from command line to represent size and initialization value
  if (argc < 3) {
    printf("Too few command-line arguments.\nUsage: %s <size> <value>\n", argv[0]);
    return 1;
  }
  int size = atoi(argv[1]);
  int value = atoi(argv[2]);

  if (size < 1) {
    printf("Size argument %d too small.  Please run with size > 0.\n", size);
    return 1;
  }
  
  printf("Creating an array with %d elements all initialized to %d.\n\n", size, value);
  

  int* list = createArray(size,value);
      
  //Output all of the values in the array that was created by the function
  //TODO - Part 3, number 1c: factor this code out into a function
  int* cur = list;
  if (cur) {
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }
  /* Uncomment when ready to test Part 3, number 1c: */
  //printArray(list, size);



  // TODO - Part 3, number 1b: fill in readArray function so this works correctly
  int list2Size = 0;
  int *list2 = NULL;
  // note that we use stdin as our filehandle, which means the function
  // will read input interactively; the function, however, shouldn't
  // care what filehandle it gets
  readArray(stdin, &list2Size, &list2);
  



  // TODO - Part 3, number 1c: replace with function call when function works
  cur = list2;
  if (cur) { // only print if cur isn't NULL (remember, NULL == 0 == false)
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }



//Change a value in the original array
  list[0] = 7;



  //Output all of the values in the original array once more (1st one may have changed)
  // TODO - Part 3, number 1c: replace with function call when function works
  cur = list;
  if (cur) { // only print if cur isn't NULL (remember, NULL = 0 = false)
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }

  //TODO - all parts: Perform any final "clean-up" needed by this program




  
  
  return 0;
}



/*
  Function that returns an array of "size" int elements,
  where each position has been initalized to "value"
     -=-=-=-=-Uses dynamic allocation!-=-=-=-=-
*/
int* createArray(int size, int value){

  //TODO - Part 3, number 1a: Fill in function defintion here
  //  Since the code won't compile if we don't return
  //  something, just return a default value.  This is
  //  a "function stub", basically it's got the right
  //  prototype, and returns the right type, but doesn't
  //  actually do anything. It's enough to let us compile
  //  without errors, though.
  //
  //  You'll need to replace this with the actual function
  //  definition.
  int *AddArray = malloc(size*sizeof(int));
  for(int i=0;i<size;i++){
    AddArray[i]= value;
  }
  return AddArray;

}


/*
  Function that returns a newly allocated array with values
  obtained from the given filehandle.  The first number
  read will be treated as the size of the new array, and
  the function will then allocate the array and attempt to
  fill it with the requested number of integers. 
*/
void readArray (FILE *fp, int *newSize, int **newArray) {

//TODO - Part 3, number 1b: 

// Below is a function stub, doesn't do anything, but makes sure
//  it reports that it hasn't done anything.
//  Fill in actual code.
//  Be sure to read from the filehandle using fscanf(); 
//  DO NOT just read from stdin using scanf().
  int* line[2];
  int i = 0;
  while (fscanf(fp,"%d",line[i]) != EOF){
    i++;
  }
  *newSize = line[0];
  *newArray = createArray(*newSize,value);
}


void printArray(int *array, int size) {

  //TODO - Part 3, number 1b: Fill in function defintion here

}

