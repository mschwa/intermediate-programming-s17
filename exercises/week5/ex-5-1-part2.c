#include <stdio.h>

void arraySwap(int** left, int** right) {
  //tmp should be a pointer to an integer because we want it to hold integer 
  //values temporarilly
  int *tmp = *left;
  *left = *right;
  *right = tmp;
}

int main() {
  int ones[] = { 1, 1, 1 };
  int twos[] = { 2, 2, 2 };

  int *p1 = ones;
  int *p2 = twos;

  /* result should be:
   * "p1: [ 2 2 2 ]
   *  p2: [ 1 1 1 ]"
   */
  arraySwap(&p1,&p2);

  printf("p1: [ ");
  for (int i=0; i<3; i++) {
    printf("%d ", p1[i]);
  }
  printf("]\n");

  printf("p2: [ ");
  for (int i=0; i<3; i++) {
    printf("%d ", p2[i]);
  }
  printf("]\n");

  return 0;
}
