#include <stdio.h>

int main() {

  /* this code should add 1 to each element of the array, 
   * then print the array.  
   * result should be: "array is [ 2 3 4 5 6 ]"
   */
  int ra[5] = { 1, 2, 3, 4, 5 };
  int *ptr = ra;
  for (int i = 0; i<5; i++) {
    *ptr = *ptr+1;
    ptr++;
  }

  printf("array is: [ ");
  for (int i=0; i<5; i++) {
    printf("%d ", ra[i]);
  }
  printf("]\n");

  return 0;
}


