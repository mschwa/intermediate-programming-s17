/* 
   In class exercise Ex-3-1.
   Practice using string functions and writing helper methods.
*/


#include <stdio.h>
#include <string.h>


/* 
   This is the function declaration (prototype) for the concat 
   function. Its definition is below.
*/
int concat(char word1[], char word2[], char result[], int resultCap);
int interleave(char str1[], char str2[], char str3[], char interleft[], int resultCap);

int main() {

    char word1[11];  //allow up to 10 chars, then room for '\0' terminator
    char word2[11];  //allow up to 10 chars, then room for '\0' terminator
    char word3[11];  //allow up to 10 chars, then room for '\0' terminator

    scanf("%s", word1);
    scanf("%s", word2);

    int resultCap;
    scanf("%d", &resultCap);
    char result[resultCap];
    char interleft[resultCap];

    //Uncomment the line below for debugging; leave it commented out in order to pass tests
    //printf("word1 is %s, word2 is %s, and resultCap is %d\n", word1, word2, resultCap);

    if(!concat(word1, word2, result, resultCap)) {
        printf("Concatenation was successful: %s\n", result);
    } else {
        printf("Concatenation was not successful.\n");
    }
    
    scanf("%s", word3);
    if (!interleave(word1, word2, word3, interleft, resultCap)){
        printf("Interleave was successful: %s\n", interleft);
    } else {
        printf("Interleave was not successful.\n");
    }
    //Uncomment the line below for debugging; leave it commented out in order to pass tests
    //printf("word1 is %s, word2 is %s, word3 is %s, resultCap is %d, and interleft is:%s\n", word1, word2, word3, resultCap, interleft);
    return 0;
}


/*
  Returns in the third argument the concatenation of the first
  argument and the second argument, provided that there is
  sufficient space in third argument, as specified by the fourth.
  e.g.
      concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
      concat("alpha", "gamma", result, 10) puts nothing into result and returns 1
 */
int concat(char word1[], char word2[], char result[], int resultCap){
    if ((int)strlen(word1) + (int)strlen(word2) < resultCap){
        int p = 0;
        //fill initial characters of result with characters of word1
        for (p=0;p<(int)strlen(word1);p++){
            result[p]=word1[p];
        }
        //fill remaining of result with word 2
        for (int i=0;i<=(int)(strlen(word2)+1);i++){  // <= accounts for null character
            result[p] = word2[i];
            p++;
        }
        return 0;
    } else {
        return 1; 
        }
    }

int interleave(char str1[], char str2[], char str3[], char interleft[], int resultCap){
    //define variables that will hold the length of the passed strings
    int l1 =(int)strlen(str1); int l2 =(int)strlen(str2); int l3 =(int)strlen(str3);
    //introduce counters that will be used to store values:
    int i = 0; int j = 0; int k = 0; int c = 0;
    /* if interleaving doesn't overflow input length, program will remain in a while 
    loop until all characteres of the passed strings are copied to interleft.*/ 
    if(l1+l2+l3 < resultCap){
        while ( c < (l1+l2+l3) ){
            if (str1[i]!='\0'){
                // if current element of correspondent string isn't null character,
                // this element will be copied to the interleft string. Otherwise,
                // the c counter won't be update and, once it reaches the next if 
                // loop, it will still be looking for a character to fill the same
                // position as before.
                interleft[c] = str1[i];
                i++; c++;
            }
           if (str2[j]!='\0'){
                interleft[c] = str2[j];
                j++; c++;
            } 
            if (str3[k]!='\0'){
                interleft[c] = str3[k];
                k++; c++;
            }
        }
        interleft[l1+l2+l3] = '\0';
        return 0;
    //if interleaving overflows cap
    } else {
        return 1;
    } 
}

