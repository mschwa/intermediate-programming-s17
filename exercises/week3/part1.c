/* 
   In class exercise Ex-3-1.
   Practice using string functions and writing helper methods.
*/


#include <stdio.h>
#include <string.h>


/* 
   This is the function declaration (prototype) for the concat 
   function. Its definition is below.
*/
int concat(char word1[], char word2[], char result[], int resultCap);



int main() {

    char word1[11];  //allow up to 10 chars, then room for '\0' terminator
    char word2[11];  //allow up to 10 chars, then room for '\0' terminator

    scanf("%s", word1);
    scanf("%s", word2);

    int resultCap;
    scanf("%d", &resultCap);
    char result[resultCap];

    //Uncomment the line below for debugging; leave it commented out in order to pass tests
    //printf("word1 is %s, word2 is %s, and resultCap is %d\n", word1, word2, resultCap);

    if(!concat(word1, word2, result, resultCap)) {
        printf("Concatenation was successful: %s\n", result);
    } else {
        printf("Concatenation was not successful.\n");
    }

    return 0;
}


/*
  Returns in the third argument the concatenation of the first
  argument and the second argument, provided that there is
  sufficient space in third argument, as specified by the fourth.
  e.g.
      concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
      concat("alpha", "gamma", result, 10) puts nothing into result and returns 1
 */
int concat(char word1[], char word2[], char result[], int resultCap){
    if ((int)strlen(word1) + (int)strlen(word2) < resultCap){
        int p = 0;
        //fill initial characters of result with characters of word1
        for (p=0;p<(int)strlen(word1);p++){
            result[p]=word1[p];
           //test:  printf("%i:%c ",p,result[p]);
        }  // at this point, p=5
        //fill remaining of result with word 2
        for (int i=0;i<=(int)(strlen(word2)+1);i++){  // <= accounts for null character
            result[p] = word2[i];
            //test: printf("%i:%c ",p,result[p]);
            p++;
        }
        //test: printf("\n");
        return 0;
    } else {
        return 1;  //replace this stub with something appropriate for function
    }
}
